import time
from sf.sf import sf
from my_utils.dataset_analysis import get_avg_word_count_per_doc
from simple_inference import dae_compress
from my_utils.rouge_folder import rouge_folder
import sys
import os


def sf_dae(input_folder, ref_folder, root_folder, model_path, compress_sf, compress_final):
    started_time = time.time()
    started_time_str = str(started_time)

    root_folder += '/' + started_time_str + '_sf_dae_' + str(compress_sf) + '_' + str(compress_final)
    sf_folder = root_folder + '/' + 'sf'
    dae_folder = root_folder + '/' + 'dae'

    os.mkdir(root_folder)
    os.mkdir(sf_folder)
    os.mkdir(dae_folder)

    sf(input_folder, sf_folder, compress_sf)
    avg_word_count_origin_doc = get_avg_word_count_per_doc(input_folder)
    print('AVG word count per original doc: {} words'.format(avg_word_count_origin_doc))
    avg_word_count_sf_doc = get_avg_word_count_per_doc(sf_folder)
    print('AVG word count per doc: {} words'.format(avg_word_count_sf_doc))

    compress_sf_real = float(avg_word_count_origin_doc) / float(avg_word_count_sf_doc)
    compress_dae_real = float(compress_final) / compress_sf_real
    if compress_dae_real < 1:
        compress_dae_real = 1
    dae_compress(model_path, sf_folder, dae_folder, compress_dae_real)
    

    # sf compress final
    sf_folder_compress_final = root_folder + '/' + 'sf_final'
    sf(input_folder, sf_folder_compress_final, compress_final)
    if ref_folder != None:
        rouge_folder(dae_folder, ref_folder, root_folder + '/rouge_final.txt')
        rouge_folder(sf_folder_compress_final, ref_folder, root_folder + '/rouge_sf_final.txt')


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('python3.7 file.py [input_folder] [ref_folder] [root_folder] [compress_x] [compress_final]')
        sys.exit()

    sf_dae(sys.argv[1], sys.argv[2], sys.argv[3], '__data/models/en_180k', float(sys.argv[4]), float(sys.argv[5]))
    # dae_sf(sys.argv[1], sys.argv[2], sys.argv[3], '__data/models/en_180k', float(sys.argv[4]), float(sys.argv[5]))
    
