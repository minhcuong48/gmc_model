# bo khoang trang thua
def remove_double_space(doc):
    while doc.find('  ') > -1:
        doc = doc.replace('  ', ' ')
    return doc

# loai bo khoang cach co do dai = 0
def remove_zero_width_space(doc):
    while doc.find('\u200B') > -1:
        doc = doc.replace('\u200B', '')
    return doc