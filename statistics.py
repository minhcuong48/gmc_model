import sys
import os

def statistics(folder, report_path):
    if not os.path.isdir(folder):
        print('{} does not exist'.format(folder))
        return

    # need
    num_of_files = 0
    num_of_words_in_docs = 0
    num_of_sents_in_docs = 0
    min_words_of_sent_in_docs = 1000000000
    max_words_of_sent_in_docs = -1
    #
    for filename in os.listdir(folder):
        num_of_files += 1
        num_of_words, num_of_sents, min_num_words_of_sent, max_num_words_of_sent = \
            get_file_info(os.path.join(folder, filename))
        num_of_words_in_docs += num_of_words
        num_of_sents_in_docs += num_of_sents
        if min_words_of_sent_in_docs > min_num_words_of_sent:
            min_words_of_sent_in_docs = min_num_words_of_sent
        if max_words_of_sent_in_docs < max_num_words_of_sent:
            max_words_of_sent_in_docs = max_num_words_of_sent
    
    print('Number of files: {}'.format(num_of_files))
    print('AVG number of words per doc: {}'.format(num_of_words_in_docs / num_of_files))
    print('AVG number of sents per doc: {}'.format(num_of_sents_in_docs / num_of_files))
    print('Max number of words per sent in docs: {}'.format(max_words_of_sent_in_docs))
    print('Min number of words per sent in docs: {}'.format(min_words_of_sent_in_docs))

    # report to file
    if os.path.exists(report_path):
        os.remove(report_path)
    f = open(report_path, 'a')
    f.write("{}:\n".format(folder))
    f.write('Number of files: {}\n'.format(num_of_files))
    f.write('AVG number of words per doc: {}\n'.format(num_of_words_in_docs / num_of_files))
    f.write('AVG number of sents per doc: {}\n'.format(num_of_sents_in_docs / num_of_files))
    f.write('Max number of words per sent in docs: {}\n'.format(max_words_of_sent_in_docs))
    f.write('Min number of words per sent in docs: {}\n\n'.format(min_words_of_sent_in_docs))
    f.close()

def get_avg_words_per_doc_in_dir(dir_path):
    if not os.path.isdir(dir_path):
        print('{} does not exist'.format(dir_path))
        return 0
    print('Calculating avg words per doc in dir: {}'.format(dir_path))
    # need
    num_of_files = 0
    num_of_words_in_docs = 0
    #
    for filename in os.listdir(dir_path):
        num_of_files += 1
        num_of_words, num_of_sents, min_num_words_of_sent, max_num_words_of_sent = \
            get_file_info(os.path.join(dir_path, filename))
        num_of_words_in_docs += num_of_words
    return float(num_of_words_in_docs) / float(num_of_files)
    
def onefile_statistics(file_path):
    if not os.path.isfile(file_path):
        print('{} is not a file'.format(file_path))
        return
    f = open(file_path, 'r')
    total_sents = 0
    total_words = 0
    min_sent = 100000000
    max_sent = -1
    for line in f:
        if len(line.strip()) <= 0:
            continue
        total_sents += 1
        words = line.split(' ')
        words = [w for w in words if len(w.strip()) > 0]
        len_words = len(words)
        total_words += len_words
        if min_sent > len_words:
            min_sent = len_words
        if max_sent < len_words:
            max_sent = len_words
        print('Reading line #{}'.format(total_sents))
    print('Number of sents: {}'.format(total_sents))
    print('Number of words: {}'.format(total_words))
    print('AVG number of words per sent: {}'.format(total_words / total_sents))
    print('MAX number of words per sent: {}'.format(max_sent))
    print('MIN number of words per sent: {}'.format(min_sent))
    f.close()

def get_file_info(file_path):
    f = open(file_path)
    doc = f.read()
    f.close()
    num_of_words = 0
    max_num_words_in_sent = -1
    min_num_words_in_sent = 10000000
    lines = doc.split("\n")
    lines = [l for l in lines if len(l.strip()) > 0]
    for l in lines:
        words = l.split(' ')
        words = [w for w in words if len(w.strip()) > 0]
        num_words_in_sent = len(words)
        num_of_words += num_words_in_sent
        if num_words_in_sent > max_num_words_in_sent:
            max_num_words_in_sent = num_words_in_sent
        if num_words_in_sent < min_num_words_in_sent:
            min_num_words_in_sent = num_words_in_sent
    # print(num_of_words, len(lines), min_num_words_in_sent, max_num_words_in_sent)
    return num_of_words, len(lines), min_num_words_in_sent, max_num_words_in_sent

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('python file.py [folder] [report_path]')
        sys.exit()

    statistics(sys.argv[1], report_path)
    
    # onefile_statistics('__data/training/cnndm_train_data')

