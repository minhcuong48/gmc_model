import time
from sf.sf import sf_by_word_count
from sf.sfsinglesummarizer import SfSingleSummarizer
from my_utils.dataset_analysis import get_avg_word_count_per_doc
from simple_inference import dae_compress
from simple_inference import run_inference_with_compression
from my_utils.rouge_folder import rouge_folder
import sys
import os


def dae_sf(input_folder, ref_folder, root_folder, model_path, compress_dae, num_word_final):
    started_time = time.time()
    started_time_str = str(started_time)

    root_folder += '/' + started_time_str + '_dae_sf_' + str(compress_dae) + '_' + str(num_word_final)
    sf_folder = root_folder + '/' + 'sf'
    dae_folder = root_folder + '/' + 'dae'
    sf_folder_compress_final = root_folder + '/' + 'sf_final'

    os.mkdir(root_folder)
    os.mkdir(sf_folder)
    os.mkdir(dae_folder)
    os.mkdir(sf_folder_compress_final)

    # begin
    count = 0
    for filename in os.listdir(input_folder):
        input_path = input_folder + '/' + filename
        out_path = dae_folder + '/' + filename
        print(input_path)
        count += 1
        print('files count: {}'.format(count))
        run_inference_with_compression(
            model_path=model_path,
            data_path=input_path,
            beam_k=None,
            output_path=out_path,
            compression=compress_dae
        )
        print("Processed by DAE: {} {} file(s)".format(filename, count))

        # sf
        summarizer = SfSingleSummarizer(dae_folder + '/' + filename, sf_folder + '/' + filename)
        summarizer.summarize_by_word_count(num_word_final)
        print('Processed by SF: {} ({})'.format(filename, count))

        # sf_final
        # sf compress final
        
        summarizer2 = SfSingleSummarizer(input_folder + '/' + filename, sf_folder_compress_final + '/' + filename)
        summarizer2.summarize_by_word_count(num_word_final)
    # end
    if ref_folder != None:
        rouge_folder(sf_folder, ref_folder, root_folder + '/rouge_final.txt')
        rouge_folder(sf_folder_compress_final, ref_folder, root_folder + '/rouge_sf_final.txt')

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('python3.7 file.py [input_folder] [ref_folder] [root_folder] [compress_x] [num_word_final]')
        sys.exit()

    dae_sf(sys.argv[1], sys.argv[2], sys.argv[3], '__data/models/en_180k', float(sys.argv[4]), float(sys.argv[5]))
    
