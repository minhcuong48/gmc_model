import sys
import struct
from tensorflow.core.example import example_pb2
import tensorflow as tf


def bin2text(in_file, out_file):
    reader = open(in_file, 'rb')
    writer = open(out_file, 'w')
    while True:
        len_bytes = reader.read(8)
        if not len_bytes:
            sys.stderr.write('Done reading\n')
            return
        str_len = struct.unpack('q', len_bytes)[0]
        tf_example_str = struct.unpack('%ds' % str_len, reader.read(str_len))[0]
        tf_example = example_pb2.Example.FromString(tf_example_str)
        examples = []
        for key in tf_example.features.feature:
            examples.append('%s=%s' % (key, tf_example.features.feature[key].bytes_list.value[0]))
        writer.write('%s\n' % '\t'.join(examples))
    reader.close()
    writer.close() 

if __name__ == '__main__':
    root = '/media/cuonggm/Data/_do_an/gmc_model/__data/download/dmcnn'

    # if len(sys.argv) != 3:
    #     print('[file.python] [input] [output]')
    #     sys.exit()

    bin2text(root + '/' + 'test.bin', root + '/' + 'test.txt')