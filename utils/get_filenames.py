import os
import sys


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('python3.7 file.py [in_folder] [num_of_files] [out_filenames.txt]')
        sys.exit()

    in_folder = sys.argv[1]
    num_of_files = int(sys.argv[2])
    out_filenames_txt = sys.argv[3]

    if os.path.isfile(out_filenames_txt):
        os.remove(out_filenames_txt)

    f = open(out_filenames_txt, 'w')
    
    num_files = 0
    for file in os.listdir(in_folder):
        f.write(file + "\n")
        num_files += 1
        print('Processing file: {}'.format(file))
        if num_files == num_of_files:
            break
    f.close()