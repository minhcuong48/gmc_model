import os
import sys
from shutil import copyfile, rmtree


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('python3.7 file.py [in_folder] [out_folder] [filenames.txt]')
        sys.exit()

    

    in_folder = sys.argv[1]
    out_folder = sys.argv[2]
    filename_txt = sys.argv[3]

    f = open(filename_txt, 'r')
    filenames = f.read().split("\n")
    f.close()
    filenames = [f for f in filenames if len(f) > 0]

    if os.path.isdir(out_folder):
        rmtree(out_folder)
    os.mkdir(out_folder)

    num_files = 0

    for file in filenames:
        copyfile(in_folder + '/' + file, out_folder + '/' + file)
        num_files += 1
        print('Copied: {}'.format(file))
        print('Copied: {}'.format(num_files))
