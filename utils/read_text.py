import sys


def read_text(path, num_lines):
    f = open(path, 'r')
    text = ''
    count_lines = 0
    for line in f:
        text += line
        count_lines += 1
        if count_lines == num_lines:
            break
    f.close()
    return text


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('[file.py] [path] [number_lines]')
        sys.exit()

    text = read_text(sys.argv[1], sys.argv[2])
    print(text)