"""
Preprocess van ban truoc khi dua vao tom tat
Bao gom:
- Loai bo cac ky tu thua
- Tach token
- Dua van ban ve dang chu thuong
- Tach thanh cac cau, moi cau 1 dong

Cac buoc tien hanh:
- Chia thanh cac lines
- Loai bo ky tu thua
- 
"""

import nltk
nltk.download('all')
from underthesea import word_tokenize

import os
import sys
import string
import re

class GmcPreprocessor:
    def remove_zero_length_whitespace(self, sentence):
        return sentence.replace("\u200B", '')

    def remove_unexpected_puncts(self, doc):
        unexpected_punct = ['(',')','{','}','[',']', '"']
        for p in unexpected_punct:
            doc = doc.replace(p, '')
        return doc
        

    def end_by_dot(self, doc):
        puncts = '.;!?:'
        for p in puncts:
            doc = doc.replace(p, '.')
        return doc

    def vi_word_tokens_str(self, sentence):
        return word_tokenize(sentence, format="text")

    def split_doc_to_lines(self, doc):
        doc = self.remove_unexpected_puncts(doc)
        doc = self.end_by_dot(doc)
        doc = self.remove_zero_length_whitespace(doc)
        sentences = doc.split("\n")
        sentences = [s.strip() for s in sentences if len(s.strip()) > 0]

        sents = []
        for s in sentences:
            s = s.split('.')
            s = [tmp.strip() for tmp in s if len(tmp.strip()) > 0]
            sents += s

        results = ''
        for s in sents:
            results += s + ".\n"
        return results

    def write_str_to_file(self, string, out_file):
        if os.path.isfile(out_file): os.remove(out_file)
        f = open(out_file, 'w')
        f.write(string)
        f.close()
        print('Wrote to {}'.format(out_file))

    @staticmethod
    def tokenize_vi(doc):
        processor = GmcPreprocessor()
        doc = processor.split_doc_to_lines(doc)
        lines = doc.split("\n")
        lines = [l for l in lines if len(l) > 0]
        doc = ''
        for line in lines:
            doc += processor.vi_word_tokens_str(line) + "\n"
        return doc.strip().lower()

    @staticmethod
    def tokenize_vi_folder(in_folder, out_folder):
        for filename in os.listdir(in_folder):
            f_read = open(in_folder + '/' + filename, 'r')
            doc = f_read.read()
            f_read.close()

            doc = GmcPreprocessor.tokenize_vi(doc)

            out_file = out_folder + '/' + filename
            if os.path.isfile(out_file): os.remove(out_file)
            f_write = open(out_file, 'w')
            f_write.write(doc)
            f_write.close()
            print('Wrote to {}'.format(out_file))

if __name__ =='__main__':
    if len(sys.argv) != 3:
        print('[file.py] [in_folder] [out_folder]')
        sys.exit()

    GmcPreprocessor.tokenize_vi_folder(sys.argv[1], sys.argv[2])
