import os
import sys


def list_files(folder, output, num_files=0):
    if os.path.isfile(output):
        os.remove(output)   
    f = open(output, 'w')
    count = 0
    for filename in os.listdir(folder):
        # bo di _xxx hau to
        index = filename.rfind('_')
        filename = filename[:index]

        print(filename)
        f.write(filename + "\n")
        count += 1
        if num_files != 0 and count == num_files:
            break
    f.close()


if __name__ == '__main__':
    if len(sys.argv) != 4 and len(sys.argv) != 3:
        print('[file.py] [folder] [out_file] [num_files]')
        sys.exit()
    
    if len(sys.argv) == 4:
        list_files(sys.argv[1], sys.argv[2], int(sys.argv[3]))

    if len(sys.argv) == 3:
        list_files(sys.argv[1], sys.argv[2])
