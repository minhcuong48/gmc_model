import os

unexpected_strings = ['<s>', '</s>', '`', '=b', "''", '"', '-lrb-', 'cnn', '-rrb-']

def clean_test_cnndm(in_file, origin_folder, ref_folder):
    f = open(in_file, 'r')
    count = 0
    for line in f:
        # print(line)
        origin, ref = get_origin_ref(line)
        write_to_files(str(count), origin, ref, origin_folder, ref_folder)
        count += 1
        print(count)
    f.close()

def get_origin_ref(line):
    parts = line.split("\tabstract")
    if len(parts) != 2:
        raise("len(parts) != 2")
    origin = remove_first_and_last_quote(parts[0])
    abstract = remove_first_and_last_quote(parts[1])

    origin = remove_unexpected_string(origin)
    abstract = remove_unexpected_string(abstract)

    origin = origin.strip()
    abstract = abstract.strip()

    origin = separate_sents_to_lines(origin)
    abstract = separate_sents_to_lines(abstract)

    return origin, abstract

def remove_first_and_last_quote(string):
    first = string.find('"')
    last = string.rfind('"')
    if first >= last:
        return string
    # print('first and last: {} {}'.format(first, last))
    return string[first+1:last]

def remove_unexpected_string(string):
    for s in unexpected_strings:
        string = string.replace(s, '')
    return string

def separate_sents_to_lines(string):
    sentences = ''
    sents = string.split('.')
    sents = [sent.lower().strip() for sent in sents if len(sent.strip()) > 2 and count_space(sent.strip()) > 2]
    for s in sents:
        sentences += s + ' .\n'
    return sentences.strip()
    
def count_space(sent):
    sent.strip()
    count = 0
    for char in sent:
        if char == ' ':
            count += 1
    return count

def write_to_files(filename, origin, ref, origin_folder, ref_folder):
    if not os.path.isdir(origin_folder):
        os.mkdir(origin_folder)

    if not os.path.isdir(ref_folder):
        os.mkdir(ref_folder)

    f1 = open(origin_folder + '/' + filename, 'w')
    f2 = open(ref_folder + '/' + filename, 'w')

    f1.write(origin)
    f2.write(ref)

    f1.close()
    f2.close()

if __name__ == '__main__':
    root = '/media/cuonggm/Data/_do_an/gmc_model/__data/download/dmcnn'
    in_file = root + '/' + 'test.txt'
    origin_folder = '__data/cnndm_test' + '/' + 'test_originals'
    ref_folder = '__data/cnndm_test' + '/' + 'test_references'
    clean_test_cnndm(in_file, origin_folder, ref_folder)