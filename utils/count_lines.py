import sys


def count_lines(in_file):
    count = 0
    f = open(in_file, 'r')
    for line in f:
        count += 1
        print(count)
    return count


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('python file.py [in_file]')
        sys.exit()

    count_lines(sys.argv[1])