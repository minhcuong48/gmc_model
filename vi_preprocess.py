import sys
import os
import shutil
from underthesea import sent_tokenize, word_tokenize, pos_tag
import common_preprocess


def vi_preprocess(in_dir, out_dir):
    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)
    os.mkdir(out_dir)

    count = 0
    for filename in os.listdir(in_dir):
        print('Preprocessing file: {}'.format(filename))
        preprocess_vi_file(in_dir, out_dir, filename)
        count += 1
        print('Preprocessed {} file(s)'.format(count))

def preprocess_vi_file(in_dir, out_dir, filename):
    f = open(os.path.join(in_dir, filename), 'r')
    doc = f.read()
    f.close()
    doc = preprocess_doc(doc)
    write_file(out_dir, filename, doc)

def preprocess_doc(doc):
    # TODO process a vi doc and return new doc as string
    # 1 - Tach cau
    sents = sent_tokenize(doc)

    # 2 - Tach word va ghep lai
    doc = ''
    for s in sents:
        words = pos_tag(s.strip())
        new_sent = ''
        for w in words:
            # neu postag la dau cau thi bo
            if w[1] in ['CH'] and w[0] != '.':
                continue
            # chay tiep
            word = word_tokenize(w[0], format='text')
            word = word.strip().lower()
            new_sent += word + ' '
        new_sent = new_sent.strip()
        doc += new_sent + '\n'
    
    # 3 - Loai bo dau cach do dai = 0
    doc = common_preprocess.remove_zero_width_space(doc)

    # 4 - Loai bo khoang trang thua
    doc = common_preprocess.remove_double_space(doc)
            
    return doc

def write_file(out_dir, filename, content):
    f = open(os.path.join(out_dir, filename), 'a')
    f.write(content)
    f.close()

# private handle



if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('python file.py [in_dir] [out_dir]')
        sys.exit()

    in_dir = sys.argv[1]
    out_dir = sys.argv[2]

    vi_preprocess(in_dir, out_dir)