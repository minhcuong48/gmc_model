def delete_fist_line(origin, dest):
    f = open(origin, 'r')
    f2 = open(dest, 'a')

    count = 0
    for line in f:
        if count == 0:
            count += 1
            continue
        f2.write(line)
        print('Wrote lines: {}'.format(count))
        count += 1
        print(line)

    f.close()
    f2.close()

if __name__ == '__main__':
    origin = '__data/gloves/gigaword_glove.txt'
    dest = '__data/gloves/gigaword_glove2.txt'

    delete_fist_line(origin, dest)
    print('END')