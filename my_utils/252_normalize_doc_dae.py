from pyvi import ViTokenizer
import os
import string


vi_inputs = './_data/others/252_docs_vi'
vi_inputs_processed = './_data/others/252_vi_processed'

vi_inputs_originals = vi_inputs + '/originals'
vi_inputs_references = vi_inputs + '/references'

vi_inputs_processed_originals = vi_inputs_processed + '/originals'
vi_inputs_processed_references = vi_inputs_processed + '/references'


# for remove unexpected puncts
puncts = string.punctuation
puncts = puncts.replace(',', '')
puncts = puncts.replace('.','')
puncts = puncts.replace(';','')
puncts = puncts.replace(':','')
puncts = puncts.replace('!','')
puncts = puncts.replace('?','')
puncts = puncts.replace("'",'')

REPLACED_CHAR = ' '

def normalize_252(inputs, outputs):
    count = 0
    for filename in os.listdir(inputs):
        print(filename)
        normalize_doc(inputs, filename, outputs)
        count += 1
        print(count)

def normalize_doc(inputs, filename, outputs):
    f = open(inputs + '/' + filename, 'r')
    doc = f.read()
    f.close()
    doc = endline_by_dot(doc)
    doc = normalize_quote(doc)
    sentences = doc.split("\n")
    sentences = [remove_puncts(sent.lower().strip()) for sent in sentences if len(sent.strip()) > 0]

    new_sentences = []
    for sent in sentences:
        sents = sent.split(".")
        sents = [s.strip() for s in sents if len(s.strip()) > 0]
        new_sentences += sents
    
    f = open(outputs + '/' + filename, 'a')

    result = ''
    for sent in new_sentences:
        # print(sent)
        result += tokenize(sent) + " .\n"
    result = remove_zero_length_whitespace(result)
    result = remove_double_spaces(result.strip())
    # print(result)
    f.write(result)
    f.close()

def tokenize(sent):
    return ViTokenizer.tokenize(sent).strip()


def remove_puncts(sent):
    for p in puncts:
        sent = sent.replace(p, REPLACED_CHAR)
    return sent

def remove_zero_length_whitespace(sent):
    return sent.replace("\u200B", "")

def remove_double_spaces(string):
    while string.find('  ') > -1:
        string = string.replace('  ', ' ')
    return string.strip()

def normalize_quote(sent):
    sent = sent.replace("\u201C", "U+0022")
    sent = sent.replace("\u201D", "U+0022")

    sent = sent.replace("\u2018", "U+0027")
    sent = sent.replace("\u2019", "U+0027")
    sent = sent.replace("\u0060", "U+0027")
    sent = sent.replace("\u00B4", "U+0027")

    return sent



def endline_by_dot(sent):
    sent = sent.replace('?', '.')
    sent = sent.replace('!', '.')
    sent = sent.replace(';', '.')
    sent = sent.replace('...', '.')
    return sent

# MAIN

# normalize_252(vi_inputs_originals, vi_inputs_processed_originals)
normalize_252(vi_inputs_references, vi_inputs_processed_references)