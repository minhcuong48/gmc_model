import os
import string


def merge_file(folder, output):
    file_count = 0
    for filename in os.listdir(folder):
        append_dailymail_file_to_other_file(folder + '/' + filename, output)
        file_count += 1
        print(file_count)

def append_dailymail_file_to_other_file(dailymail_file, destination_file):
    f = open(dailymail_file, 'r')
    doc = f.read()
    highlight_index = doc.find('@highlight')
    if highlight_index > -1:
        doc = doc[:highlight_index]
    doc = doc.strip()
    lines = doc.split("\n\n")
    for line in lines:
        line = line.replace("\n", " ")
        line = separate_puncts(line)
        while line.find("  ") > -1:
            line = line.replace("  ", " ")
        line += "\n"
        line = line.lower()
        line.strip()
        append_line_to_file(line, destination_file)
    # append_string_to_file(doc, destination_file)

def separate_puncts(line):
    for punct in string.punctuation:
        line = line.replace(punct, f" {punct} ")
    return line

def append_string_to_file(string, filename):
    f = open(filename, 'a')
    f.write(string)
    f.close
    print(f'Appended to {filename}')

def append_line_to_file(line, file, log=False):
    f = open(file, 'a')
    f.write(line)
    f.close
    if log == True:
        print(f'Appended to {line}')

def read_3_lines(filepath):
    f = open(filepath, 'r')
    for i in range(3):
        line = f.readline()
        print(line, end="")
    f.close()


# MAIN

merge_file('/media/cuonggm/Window 10/_do_an_dataset/dailymail_stories/dailymail/stories', '/media/cuonggm/Window 10/_do_an_dataset/dailymail_stories/dailymail/dailymail_training.txt')

# read_3_lines('/media/cuonggm/Window 10/_do_an_dataset/dailymail_stories/dailymail/dailymail_training.txt')
