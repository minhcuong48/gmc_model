"""
Script nay dung de tach moi dong thanh cac cau sao cho dung.
"""

import os
import sys
import string
import re


# for remove unexpected puncts
puncts = string.punctuation
puncts = puncts.replace(',', '')
puncts = puncts.replace('.','')
puncts = puncts.replace(';','')
puncts = puncts.replace(':','')
puncts = puncts.replace('!','')
puncts = puncts.replace('?','')
puncts = puncts.replace("'",'')

def split_line_to_sentences(folder, output_folder):
    count = 0
    for filename in os.listdir(folder):
        split_in_a_file(folder, filename, output_folder)
        count += 1
        print(count)


def split_in_a_file(folder, filename, output_folder):
    f = open(folder + '/' + filename, 'r')
    list_sents = []
    for line in f:
        line = line.strip()
        if line == '':
            continue
        
        line = normalize_quote(line)
        line = remove_puncts(line)
        line = remove_zero_length_whitespace(line)
        line = remove_double_spaces(line)
        sents = line.split(' . ')
        sents = split_by(sents, '!')
        sents = split_by(sents, '?')
        sents = split_by(sents, ';')
        sents = [sent.lower().strip() for sent in sents if len(sent.strip()) > 1]
        list_sents += sents
    f.close()
    f = open(output_folder + '/' + filename, 'a')
    for sent in list_sents:
        f.write(sent)
        if not sent[-1] in string.punctuation:
            f.write(' .')
        f.write("\n")
    f.close()


def split_by(sents, char):
    new_sents = []
    for sent in sents:
        sentences = sent.split(char)
        sentences = [s.lower().strip() for s in sentences if len(s.strip()) > 1]
        new_sents += sentences
    return new_sents
# preprocess



REPLACED_CHAR = ' '
def remove_puncts(sent):
    for p in puncts:
        sent = sent.replace(p, REPLACED_CHAR)
    return sent

def remove_zero_length_whitespace(sent):
    return sent.replace("\u200B", "")

def remove_double_spaces(string):
    while string.find('  ') > -1:
        string = string.replace('  ', ' ')
    return string.strip()

def normalize_quote(sent):
    sent = sent.replace("\u201C", "U+0022")
    sent = sent.replace("\u201D", "U+0022")

    sent = sent.replace("\u2018", "U+0027")
    sent = sent.replace("\u2019", "U+0027")
    sent = sent.replace("\u0060", "U+0027")
    sent = sent.replace("\u00B4", "U+0027")

    return sent

def endline_by_dot(sent):
    sent = sent.replace('?', '.')
    sent = sent.replace('!', '.')
    sent = sent.replace(';', '.')
    sent = sent.replace('...', '.')
    return sent

# MAIN

if len(sys.argv) != 3:
    print('<exec_file> <inputs> <outputs>')
    sys.exit()

split_line_to_sentences(sys.argv[1], sys.argv[2])