from rouge import Rouge
import sys

if len(sys.argv) != 3:
    print('Expected 2 arguments!')
    sys.exit()

f1 = open(sys.argv[1],'r')
f2 = open(sys.argv[2], 'r')

system = f1.read()
ref = f2.read()

rouge = Rouge()
scores = rouge.get_scores(system, ref)

print(scores)