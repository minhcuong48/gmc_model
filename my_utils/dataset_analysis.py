import os
import sys

def analysis(folder):
    files = []
    for filename in os.listdir(folder):
        # print('Start process: {}'.format(filename))
        # ignore systemfile
        if filename[0] == '.':
            continue
        # get lines count and word count
        lines_count, words_count = analysis_file(folder + '/' + filename)
        files.append({'filename': filename, 'lines': lines_count, 'words': words_count})
    read_info(files)

def analysis_file(path):
    f = open(path, 'r')
    lines_count, words_count = 0, 0
    for line in f:
        line = line.strip()
        if line == '':
            continue
        lines_count += 1
        words = line.split(' ')
        words = [word for word in words if len(word.strip())  > 0]
        words_count += len(words)
    return lines_count, words_count

def read_info(files):
    print('Number of files: {}'.format(len(files)))
    total_sent, total_word = 0, 0
    max_sent_length = -1
    min_sent_length = 10000.0
    max_file_length = -1
    min_file_length = 100000
    for file in files:
        total_sent += file['lines']
        total_word += file['words']
        if max_sent_length < file['words']:
            max_sent_length = file['words']
        if min_sent_length > file['words']:
            min_sent_length = file['words']  
    print('Number of sentences: {}'.format(total_sent))
    print('Number of tokens: {}'.format(total_word))
    print('1 file = {} sentences = {} words'.format(total_sent/len(files), total_word/len(files)))
    print('1 sentences = {} words'.format(total_word/total_sent))
    print('max sent length = {}'.format(max_sent_length))
    print('min sent length = {}'.format(min_sent_length))
    print("\n")

def get_avg_word_count_per_doc(folder):
    files = []
    for filename in os.listdir(folder):
        # print('Start process: {}'.format(filename))
        # ignore systemfile
        if filename[0] == '.':
            continue
        # get lines count and word count
        lines_count, words_count = analysis_file(folder + '/' + filename)
        files.append({'filename': filename, 'lines': lines_count, 'words': words_count})
    
    # print('Number of files: {}'.format(len(files)))
    total_sent, total_word = 0, 0
    for file in files:
        total_sent += file['lines']
        total_word += file['words']
    # print('Number of sentences: {}'.format(total_sent))
    # print('Number of tokens: {}'.format(total_word))
    # print('1 file = {} sentences = {} words'.format(total_sent/len(files), total_word/len(files)))
    # print('1 sentences = {} words'.format(total_word/total_sent))
    return total_word/len(files)   


# MAIN
if __name__ =='__main__':
    if len(sys.argv) != 2:
        print('python file.py [folder]')
        sys.exit()

    analysis(sys.argv[1])