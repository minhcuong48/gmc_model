"""
Script nay de normalize corpus "cnndm_train_txt_src"
File goc da tach thanh cac token cach nhau boi khoang trang
Tuy nhien chua tach moi dong thanh 1 cau.
Giua cac dong van con khoang trang "\n"
"""

import string


cnndm_src_path = './_data/training/cnndm_train_txt_src'
cnndm_train_path = './_data/training/cnndm_train_data'

puncts = string.punctuation
puncts = puncts.replace(",", "")
puncts = puncts.replace(".","")
puncts = puncts.replace(";","")
puncts = puncts.replace(":","")
puncts = puncts.replace("!","")
puncts = puncts.replace("?","")
puncts = puncts.replace("'","")

REPLACED_CHAR = ""

print(puncts)
print(len(puncts))

def normalize(cnndm_src_path, cnndm_train_path):
    byte_count = 0.0

    f = open(cnndm_src_path, 'r')
    for line in f:
        byte_count += len(line)
        line = line.strip()
        if line == "":
            continue
        # remove puncts
        for p in puncts:
            line = line.replace(p, REPLACED_CHAR)
            
        #
        line = line.replace("''", "")
        line = remove_double_spaces(line)

        lines = line.split(".")
        lines = [line.lower().strip() for line in lines if len(line.strip()) > 0]
        string = ''
        for line in lines:
            string += line + " .\n"
        write_str_to_file(string, cnndm_train_path)
        print('Processed: {} MB'.format(byte_count / 1024 / 1024))

def write_str_to_file(string, cnndm_train_path):
    f = open(cnndm_train_path, 'a')
    f.write(string)
    f.close()

def remove_double_spaces(string):
    while string.find("  ") > -1:
        string = string.replace("  ", " ")
    return string.strip()


# MAIN

normalize(cnndm_src_path, cnndm_train_path)