import os
import nltk
nltk.download('all')

"""
Script nay thieu tach cac cau trong 1 dong
"""

# DEFINE

def split_original_highlight_folder(folder, originals_folder, references_folder):
    count = 0
    for file in os.listdir(folder):
        print(folder + '/' + file)
        split_original_highlight_doc(stories_raw_folder, file, originals_folder, references_folder)
        count += 1
        print(count)
        # if count == 100:
        #     break

def split_original_highlight_doc(folder, filename, originals_folder, references_folder):
    # open doc
    f = open(folder + '/' + filename, 'r')
    # read all doc
    doc = f.read()
    # find end of original
    index = doc.find('@highlight')
    original = doc[:index].strip()
    highlights = doc[index:].strip()

    original_lines = original.split("\n\n")
    original_lines = [line.lower().replace("\n", " ").strip() for line in original_lines if len(line.strip()) > 0]
    write_original_to_file(original_lines, filename, originals_folder)

    highlights = highlights.split('@highlight')
    highlights = [highlight.lower().strip() for highlight in highlights if len(highlight.strip()) > 0]
    write_reference_to_file(highlights, filename, references_folder)
    f.close()

def tokenized_sent(sentence):
    new_sent = ''
    tokens = nltk.word_tokenize(sentence)
    for token in tokens:
        new_sent += token + ' '
    return new_sent.strip()


def write_original_to_file(original_lines, filename, originals_folder):
    filename = filename.replace('.story', '')
    f = open(originals_folder + '/' + filename, 'w')
    for line in original_lines:
        f.write(tokenized_sent(line) + '\n')
    f.close()

def write_reference_to_file(highlights, filename, references_folder):
    filename = filename.replace('.story', '_reference')
    f = open(references_folder + '/' + filename, 'w')
    for highlight in highlights:
        f.write(tokenized_sent(highlight) + '\n')
    f.close()
        


# MAIN

# Set folder paths
stories_raw_folder = './_data/others/dailymail_stories_raw'
originals_folder = './_data/others/dailymail_stories/originals'
references_folder = './_data/others/dailymail_stories/references'

split_original_highlight_folder(stories_raw_folder, originals_folder, references_folder)