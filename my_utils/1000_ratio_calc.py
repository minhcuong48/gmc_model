import os
import nltk

root = '/media/cuonggm/Data/_do_an/gmc_model/_data/others/dailymail_stories_1000'
originals = root + '/' + 'originals'
references = root + '/' + 'references'

origin_word_count = 0
origin_sent_count = 0
ref_word_count = 0
ref_sent_count = 0
count = 0
for filename in os.listdir(originals):
    # calc ratio = ref_word_count / original_word_count
    f = open(originals + '/' + filename, 'r')
    origin = f.read()
    f.close()

    f = open(references + '/' + filename + '_reference')
    ref = f.read()
    f.close()

    origin_tokens = nltk.word_tokenize(origin)
    ref_tokens = nltk.word_tokenize(ref)

    origin_lines = origin.split("\n")
    ref_lines = ref.split("\n")

    origin_sent_count += len(origin_lines)
    ref_sent_count += len(ref_lines)

    origin_count = len(origin_tokens)
    ref_count = len(ref_tokens)

    origin_word_count += origin_count
    ref_word_count += ref_count

    count += 1

origin_word_count /= count
ref_word_count /= count
word_ratio = ref_word_count / origin_word_count
origin_sent_count /= count
ref_sent_count /= count
sent_ratio = ref_sent_count / origin_sent_count

print('AVG origin_word_count = {}'.format(origin_word_count))
print('AVG ref_word_count = {}'.format(ref_word_count))
print('AVG word_ratio = {}'.format(word_ratio))
print('AVG origin_sent_count = {}'.format(origin_sent_count))
print('AVG ref_sent_count = {}'.format(ref_sent_count))
print('AVG sent_ratio = {}'.format(sent_ratio))