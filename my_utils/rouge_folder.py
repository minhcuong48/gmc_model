from rouge import Rouge
import os
import sys

def rouge_folder(system_folder, ref_folder, out_file):

    rouge = Rouge()
    rouges = []
    count = 0

    avg_rouge1_r = 0.0
    avg_rouge1_p = 0.0
    avg_rouge1_f = 0.0

    avg_rouge2_r = 0.0
    avg_rouge2_p = 0.0
    avg_rouge2_f = 0.0

    avg_rougel_r = 0.0
    avg_rougel_p = 0.0
    avg_rougel_f = 0.0


    for filename in os.listdir(system_folder):
        f = open(system_folder + '/' + filename, 'r')
        system = f.read()
        f.close()

        f = open(ref_folder + '/' + filename.split("_")[0], 'r')
        ref = f.read()
        f.close()

        current_rouge = rouge.get_scores(system, ref)
        rouges.append(current_rouge)
        # print(filename.split("_")[0])
        count += 1
        # print(count)

        # calc avg rouge
        avg_rouge1_r += current_rouge[0]['rouge-1']['r']
        avg_rouge1_p += current_rouge[0]['rouge-1']['p']
        avg_rouge1_f += current_rouge[0]['rouge-1']['f']

        avg_rouge2_r += current_rouge[0]['rouge-2']['r']
        avg_rouge2_p += current_rouge[0]['rouge-2']['p']
        avg_rouge2_f += current_rouge[0]['rouge-2']['f']

        avg_rougel_r += current_rouge[0]['rouge-l']['r']
        avg_rougel_p += current_rouge[0]['rouge-l']['p']
        avg_rougel_f += current_rouge[0]['rouge-l']['f']

    # divide by count
    avg_rouge1_r /= count
    avg_rouge1_p /= count
    avg_rouge1_f /= count

    avg_rouge2_r /= count
    avg_rouge2_p /= count
    avg_rouge2_f /= count

    avg_rougel_r /= count
    avg_rougel_p /= count
    avg_rougel_f /= count

    if os.path.isfile(out_file):
        os.remove(out_file)

    f = open(out_file, 'w')

    f.write("Rouge 1: R: {}\n".format(avg_rouge1_r))
    f.write("Rouge 1: P: {}\n".format(avg_rouge1_p))
    f.write("Rouge 1: F: {}\n".format(avg_rouge1_f))

    f.write("Rouge 2: R: {}\n".format(avg_rouge2_r))
    f.write("Rouge 2: P: {}\n".format(avg_rouge2_p))
    f.write("Rouge 2: F: {}\n".format(avg_rouge2_f))

    f.write("Rouge L: R: {}\n".format(avg_rougel_r))
    f.write("Rouge L: P: {}\n".format(avg_rougel_p))
    f.write("Rouge L: F: {}\n".format(avg_rougel_f))

    f.close()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('<exec_file> <sys_folder> <ref_folder>')
        sys.exit()

    # system_folder = './_data/textrank_6_sents_compress_2_6'
    # ref_folder = './_data/others/dailymail_stories_1000/references_clean'

    system_folder = sys.argv[1]
    ref_folder = sys.argv[2]

    rouge = Rouge()
    rouges = []
    count = 0

    avg_rouge1_r = 0.0
    avg_rouge1_p = 0.0
    avg_rouge1_f = 0.0

    avg_rouge2_r = 0.0
    avg_rouge2_p = 0.0
    avg_rouge2_f = 0.0

    avg_rougel_r = 0.0
    avg_rougel_p = 0.0
    avg_rougel_f = 0.0


    for filename in os.listdir(system_folder):
        f = open(system_folder + '/' + filename, 'r')
        system = f.read()
        f.close()

        f = open(ref_folder + '/' + filename, 'r')
        ref = f.read()
        f.close()

        current_rouge = rouge.get_scores(system, ref)
        rouges.append(current_rouge)
        # print(filename.split("_")[0])
        count += 1
        # print(count)

        # calc avg rouge
        avg_rouge1_r += current_rouge[0]['rouge-1']['r']
        avg_rouge1_p += current_rouge[0]['rouge-1']['p']
        avg_rouge1_f += current_rouge[0]['rouge-1']['f']

        avg_rouge2_r += current_rouge[0]['rouge-2']['r']
        avg_rouge2_p += current_rouge[0]['rouge-2']['p']
        avg_rouge2_f += current_rouge[0]['rouge-2']['f']

        avg_rougel_r += current_rouge[0]['rouge-l']['r']
        avg_rougel_p += current_rouge[0]['rouge-l']['p']
        avg_rougel_f += current_rouge[0]['rouge-l']['f']

    # divide by count
    avg_rouge1_r /= count
    avg_rouge1_p /= count
    avg_rouge1_f /= count

    avg_rouge2_r /= count
    avg_rouge2_p /= count
    avg_rouge2_f /= count

    avg_rougel_r /= count
    avg_rougel_p /= count
    avg_rougel_f /= count

        
    print('Rouge 1: R: {}'.format(avg_rouge1_r))
    print('Rouge 1: P: {}'.format(avg_rouge1_p))
    print('Rouge 1: F: {}'.format(avg_rouge1_f))

    print('Rouge 2: R: {}'.format(avg_rouge2_r))
    print('Rouge 2: P: {}'.format(avg_rouge2_p))
    print('Rouge 2: F: {}'.format(avg_rouge2_f))

    print('Rouge L: R: {}'.format(avg_rougel_r))
    print('Rouge L: P: {}'.format(avg_rougel_p))
    print('Rouge L: F: {}'.format(avg_rougel_f))