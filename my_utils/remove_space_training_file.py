def remove_space_training_file(input, output):
    f = open(input, 'r')
    o = open(output, 'a')
    line_count = 0
    for line in f:
        if line == "":
            continue
        line = line.strip()
        o.write(line)
        line_count += 1
        print(line_count)
    f.close()
    o.close()


# MAIN

remove_space_training_file('/media/cuonggm/Window 10/_do_an_dataset/dailymail_stories/dailymail/dailymail_training.txt', '/media/cuonggm/Window 10/_do_an_dataset/dailymail_stories/dailymail/dailymail_training_clean.txt')
