import os
from shutil import copyfile


root = '/media/cuonggm/Data/_do_an/gmc_model/_data/others/dailymail_stories'
originals = root + '/' + 'originals'
references = root + '/' + 'references'

des = '/media/cuonggm/Data/_do_an/gmc_model/_data/others/dailymail_stories_1000'
des_originals = des + '/' + 'originals'
des_references = des + '/' + 'references'

count = 0
for filename in os.listdir(originals):
    copyfile(originals + '/' + filename, des_originals + '/' + filename)
    copyfile(references + '/' + filename + '_reference', des_references + '/' + filename + '_reference')

    count += 1
    print(count)
    if count == 1000:
        break