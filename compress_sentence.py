import os
import time
import sys
from simple_inference import dae_compress
import shutil

def compress_sentence(source, ratio, model):
    current_time = time.time()
    current_time_str = str(current_time)

    tmp_file_origin = '__data/tmp/tmp/' + current_time_str
    tmp_file_origin_compressed = '__data/tmp/tmp/' + current_time_str + '_compressed'

    os.mkdir(tmp_file_origin)
    os.mkdir(tmp_file_origin_compressed)
    write_sent(source, tmp_file_origin + '/' + 'sentence')
    dae_compress(model, tmp_file_origin, tmp_file_origin_compressed, ratio)
    compressed_sent = read_sent(tmp_file_origin_compressed + '/sentence')
    shutil.rmtree(tmp_file_origin)
    shutil.rmtree(tmp_file_origin_compressed)
    return compressed_sent

def write_sent(sent, file):
    f = open(file, 'w')
    f.write(sent)
    f.close()

def read_sent(file):
    f = open(file, 'r')
    sent = f.read()
    f.close()
    return sent

if __name__ == '__main__':
    sent = 'I love u so much my girl, i wanna stay with u in my whole life'
    compressed_sentence = compress_sentence(sent, 2, '__data/models/en_180k')
    print(compressed_sentence)