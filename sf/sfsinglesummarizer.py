import os
import sys
from sf.sentence import Sentence

class SfSingleSummarizer:
    def __init__(self, in_file, out_file):
        self.in_file = in_file
        self.out_file = out_file

    def preprocess(self):
        f = open(self.in_file, 'r')
        doc = f.read()
        f.close()
        self.doc = doc

        sents = doc.split("\n")
        sents = [sent.strip() for sent in sents if len(sent.strip()) > 0]
        
        self.sentences = []
        self.dict_token_count = {}
        index = 0
        for sent in sents:
            sentence = Sentence(index, sent)
            self.sentences.append(sentence)
            index += 1

    def prepare_data_for_extract_features(self):
        # tinh trong so trung binh cau, ta can tinh trong so cua tung token
        # dem tan suat cua cac token trong doc
        self.dict_token_count = {}
        self.list_tokens = []
        for s in self.sentences:
            for t in s.tokens:
                self.list_tokens.append(t)
                if t in self.dict_token_count.keys():
                    self.dict_token_count[t] += 1
                else:
                    self.dict_token_count[t] = 1
        
        # tinh trong so cua cac token do; dao nguoc tan suat xuat hien.
        # tan suat cang nho thi trong so cang lon
        max_freq = -1
        min_freq = 1000
        for key, value in self.dict_token_count.items():
            if value > max_freq: max_freq = value
            if value < min_freq: min_freq = value
        self.dict_token_weight = {}
        for key, value in self.dict_token_count.items():
            self.dict_token_weight[key] = min_freq + max_freq - self.dict_token_count[key]
        for key, value in self.dict_token_weight.items():
            self.dict_token_weight[key] = (float(self.dict_token_weight[key])-min_freq)/(float(max_freq)-float(min_freq))


    def extract_features(self):
        self.asw()
        self.iss()
        self.nd()
        self.sl()
        self.sp()

    def scoring(self):
        # can chuan hoa truoc khi tinh ra score
        for s in self.sentences:
            s.score = (s.asw + s.iss + s.nd + s.sl + s.sp)/5
        # sap xep
        self.sentences.sort(key = lambda _:_.score, reverse=True)
        # for s in self.sentences:
        #     print('score={};value={}'.format(s.score, s.value[:100]))

    def get_summary(self, compression):
        self.summary = []

        num_tokens = 0
        for s in self.sentences:
            num_tokens += len(s.tokens)

        num_word = int(float(num_tokens) / compression) + 1
        # print('num_words = {}'.format(num_word))
        count_word = 0
        for s in self.sentences:
            count_word += len(s.tokens)
            if(count_word > num_word):
                break
            self.summary.append(s)
        if len(self.summary) <= 0:
            self.summary.append(self.sentences[0])
        
        self.summary.sort(key=lambda _:_.id, reverse=False)
        # for s in self.summary:
        #     print('score={};id={};value={}'.format(s.score, s.id, s.value[:100]))

    def get_summary_by_word_count(self, word_count):
        self.summary = []

        count_word = 0
        for s in self.sentences:
            count_word += len(s.tokens)
            if(count_word >= word_count):
                break
            self.summary.append(s)
        if len(self.summary) <= 0:
            self.summary.append(self.sentences[0])
        
        self.summary.sort(key=lambda _:_.id, reverse=False)
        # for s in self.summary:
        #     print('score={};id={};value={}'.format(s.score, s.id, s.value[:100]))
        
    def write(self):
        if os.path.isfile(self.out_file): os.remove(self.out_file)
        f = open(self.out_file, 'w')
        for s in self.summary:
            f.write(s.value + "\n")
        f.close()
        print('Wrote sf summary to {}'.format(self.out_file))

    def summarize(self, compression):
        self.preprocess()
        self.prepare_data_for_extract_features()
        self.extract_features()
        self.scoring()
        self.get_summary(compression)
        self.write()

    def summarize_by_word_count(self, word_count):
        self.preprocess()
        self.prepare_data_for_extract_features()
        self.extract_features()
        self.scoring()
        self.get_summary_by_word_count(word_count)
        self.write()

    # features
    def asw(self):
        for s in self.sentences:
            s.asw = 0.0
            for t in s.tokens:
                s.asw += self.dict_token_weight[t]
        # chuan hoa ve [0, 1]
        max = -1
        min = 1000
        for s in self.sentences:
            if max < s.asw: max = s.asw
            if min > s.asw: min = s.asw
        for s in self.sentences:
            if max != min:
                s.asw = (s.asw-min)/(max-min)
            else:
                s.asw = 0.0 
        # for s in self.sentences:
        #     print('s.asw={}'.format(s.asw))
    
    def iss(self):
        for s in self.sentences:
            current_tokens = set(s.tokens)
            other_sents = [sent for sent in self.sentences if sent.id != s.id]
            other_sents_tokens = set([])
            for sent in other_sents:
                other_sents_tokens = other_sents_tokens.union(set(sent.tokens))
            intersec_tokens = current_tokens.intersection(other_sents_tokens)
            union_tokens = current_tokens.union(other_sents_tokens)
            s.iss = float(len(intersec_tokens)) / float(len(union_tokens))
        # chuan hoa ve [0, 1]
        max = -1
        min = 1000
        for s in self.sentences:
            if max < s.iss: max = s.iss
            if min > s.iss: min = s.iss
        for s in self.sentences:
            if max != min:
                s.iss = (s.iss-min)/(max-min)
            else:
                s.iss = 0.0 
        # for s in self.sentences:
        #     print('s.iss={}'.format(s.iss))
            
    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            pass
    
        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass
    
        return False

    def nd(self):
        for s in self.sentences:
            num_of_datanum = 0.0
            for t in s.tokens:
                if self.is_number(t): num_of_datanum += 1
            s.nd = num_of_datanum / len(s.tokens)
        # chuan hoa ve [0, 1]
        max = -1
        min = 1000
        for s in self.sentences:
            if max < s.nd: max = s.nd
            if min > s.nd: min = s.nd
        for s in self.sentences:
            if max != min:
                s.nd = (s.nd-min)/(max-min)
            else:
                s.nd = 0.0 
        # for s in self.sentences:
        #     print('s.nd={}'.format(s.nd))

    def sl(self):
        for s in self.sentences:
            s.sl = len(s.tokens)
        # chuan hoa ve [0, 1]
        max = -1
        min = 1000
        for s in self.sentences:
            if max < s.sl: max = s.sl
            if min > s.sl: min = s.sl
        for s in self.sentences:
            if max != min:
                s.sl = (s.sl-min)/(max-min)
            else:
                s.sl = 0.0 
        # for s in self.sentences:
        #     print('s.sl={}'.format(s.sl))

    def sp(self):
        for s in self.sentences:
            s.sp = len(self.sentences) - s.id
        # chuan hoa ve [0, 1]
        max = -1
        min = 1000
        for s in self.sentences:
            if max < s.sp: max = s.sp
            if min > s.sp: min = s.sp
        for s in self.sentences:
            if max != min:
                s.sp = (s.sp-min)/(max-min)
            else:
                s.sp = 0.0 
        # for s in self.sentences:
        #     print('s.slp={}'.format(s.sp))

    
if __name__ == '__main__':
    if len(sys.argv) != 3:
        sys.exit()
    summarizer = SfSingleSummarizer(sys.argv[1], sys.argv[2])
    summarizer.summarize(5)