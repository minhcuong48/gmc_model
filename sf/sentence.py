class Sentence:
    def __init__(self, id, value):
        self.id = id
        self.value = value
        self.tokens = value.split(' ')
        self.tokens = [t.strip() for t in self.tokens if len(t.strip()) > 0]