# Giới thiệu

Mô hình này kết hợp các phương pháp tóm tắt trích rút câu để cải tiến phương pháp nén câu (thuần abstractive).
Kết quả cải thiện từ 2,9% đến 12,1% trên dữ liệu Tiếng Anh và Tiếng Việt.

## Quickstart

### Step 1: Cài đặt môi trường

1. Mã nguồn này được thử nghiệm trên Ubuntu 16.04 LTS
2. Yêu cầu: python 3.7, pip, java 1.8
3. Cài đặt các thư viện python liên quan: pip install -r requirements.txt


### Step 2: Huấn luyện cho mô hình nén câu

1. Dữ liệu huấn luyện là 1 file text, trong đó mỗi câu nằm trên một dòng, các token trong câu (bao gồm cả dấu chấm, phẩy ...) đều cách nhau 1 khoảng trắng.
2. Cấu hình đường dẫn các thư mục: models, vocabs, logs, WordEmbedding và cài đặt các hyperparams tại các file: env_configs/env_config.json, runs/en.json (Dành cho Tiếng Anh), runs/vi.json (dành cho Tiếng Việt). Mô hình này cần cả mô hình W2V hoặc GloVe đã được huấn luyện.
3. Tạo từ vựng: python src/datasets/preprocess.py train_data_file output_voc_file
4. Huấn luyện: python sample_scripts/dae_json.py runs/default/en.json (cho tiếng Anh)

### Step 3: Chạy test

1. Trích rút Text Rank: java -jar textrank.jar <thư mục vào> <thư mục ra> <tỉ lệ nén>
2. Trích rút sử dụng đặc trưng câu: python sf.py <thư mục vào> <thư mục ra> <tỉ lệ nén>
3. Nén câu: python simple_inference.py <model> <thư mục vào> <thư mục ra> <tỉ lệ nén>

### Step 5: Đánh giá bằng Rouge
Đánh giá rouge trung bình cho cả thư mục: python utils/rouge_filenames.py <thư mục tóm tắt> <thư mục tham chiếu>
Trong đó: thư mục tóm tắt là nơi chứa các bản tóm tắt bằng hệ thống, thư mục tham chiếu là nơi chứa các văn bản tham chiếu tương ứng (thương do con người tóm tắt).

## References:
Kỹ thuật nén câu: https://github.com/zphang/usc_dae
