"""
Xu ly bo corpus bao moi co Trang gui thanh input cua DAE - nen cau
Moi cau la 1 dong
"""

import json
import time
import os
import string

def convert_corpus():
    # load setting
    f = open('setting_vi_corpus.json', 'r')
    setting = json.load(f)

    # params
    in_folder = setting['in_folder']
    out_file = setting['out_file']
    max_read_files = setting['max_read_files']

    print(setting)

    # handle
    # check if output exist then delete
    if os.path.isfile(out_file):
        os.remove(out_file)

    count = 0
    for filename in os.listdir(in_folder):
        print('Processing file: {}'.format(filename))
        print('Processed: {} files'.format(count))
        f = open(in_folder + '/' + filename, 'r')
        doc = f.read()
        f.close()
        doc = process(doc)
        write_to_out_file(doc, out_file)
        count += 1

        if max_read_files != 0 and count == max_read_files:
            break

def process(doc):
    lines = doc.split("\n")
    lines = [line.lower().strip() for line in lines if len(line.strip()) > 1]
    new_sents = []
    for line in lines:
        sents = line.split('.')
        sents = [sent.lower().strip() for sent in sents if len(sent.strip()) > 1]
        new_sents += sents

    doc = ''
    for sent in new_sents:
        sent = remove_unexpected_puncts(sent)
        if len(sent.split(' ')) > 8:
            doc += sent + " .\n"

    return doc.strip()

def remove_unexpected_puncts(source_str):
    allowed_puncts = "./_-"
    removed_puncts = string.punctuation
    for p in allowed_puncts:
        removed_puncts = removed_puncts.replace(p, '')
    for p in removed_puncts:
        source_str = source_str.replace(p, '')

    # remove cac dau nhay don nhay kep ko hop le
    source_str = source_str.replace("\u0060", '')
    source_str = source_str.replace("\u00B4", '')
    source_str = source_str.replace("\u2018", '')
    source_str = source_str.replace("\u2019", '')
    source_str = source_str.replace("\u201C", '')
    source_str = source_str.replace("\u201D", '')

    # zero width whitespace
    source_str = source_str.replace("\u200B", '')

    # remove double space
    while source_str.find('  ') > -1:
        source_str = source_str.replace('  ', ' ')
    
    return source_str.strip()


def write_to_out_file(doc, out_file):
    f = open(out_file, 'a')
    f.write(doc)
    f.write("\n")
    f.close()
        

def timestamp_str():
    return str(time.time())


if __name__ == '__main__':
    convert_corpus()