import os
import sys
from compress_sentence import compress_sentence


def compress_folder(folder, out_folder, model, sent_max_leng):
    for filename in os.listdir(folder):
        print(filename)
        f = open(folder + '/' + filename, 'r')
        doc = ''
        for line in f:
            line = line.strip()
            sent_leng = len(line.split(' '))
            if sent_leng > sent_max_leng:
                ratio = sent_leng / sent_max_leng
                line = compress_sentence(line, ratio, model)
            doc += line
        f.close()
        f = open(out_folder + '/' + filename, 'w')
        f.write(doc)
        f.close()
        print('Processed {}'.format(filename))


if __name__ == "__main__":
    compress_folder('__data/tests/cnndm_ab_tr2.0', '__data/tests/cnndm_ab_tr2.0_maxword8', '__data/models/en_180k', 20)