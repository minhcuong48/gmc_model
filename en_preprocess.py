import sys
import os
import shutil
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk import pos_tag
import common_preprocess

def en_preprocess(in_dir, out_dir):
    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)
    os.mkdir(out_dir)

    count = 0
    for filename in os.listdir(in_dir):
        print('Preprocessing file: {}'.format(filename))
        preprocess_en_file(in_dir, out_dir, filename)
        count += 1
        print('Preprocessed {} file(s)'.format(count))
        # if count == 10:
        #     break

def preprocess_en_file(in_dir, out_dir, filename):
    f = open(os.path.join(in_dir, filename), 'r')
    doc = f.read()
    f.close()
    doc = preprocess_doc(doc)
    write_file(out_dir, filename, doc)

def preprocess_doc(doc):
    # TODO process a en doc and return new doc as string
    # 1 - Tach cau
    sents = sent_tokenize(doc)
    doc = ''
    # 2 - Tach tu
    for s in sents:
        words = word_tokenize(s)
        tags = pos_tag(words)
        new_sent = ''
        for tag in tags:
            if tag[1] in [',', '-', ':', '*', '<', '>', '(', ')', '[', ']'] or tag[0] in ["'", "''", '`']:
                continue
            new_sent += tag[0] + ' '
        doc += new_sent.strip().lower() + '\n'

    # 3 - loai bo zero width space
    doc = common_preprocess.remove_zero_width_space(doc)
    doc = common_preprocess.remove_double_space(doc)
    return doc

def write_file(out_dir, filename, content):
    f = open(os.path.join(out_dir, filename), 'a')
    f.write(content)
    f.close()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('python file.py [in_dir] [out_dir]')
        sys.exit()

    in_dir = sys.argv[1]
    out_dir = sys.argv[2]

    en_preprocess(in_dir, out_dir)