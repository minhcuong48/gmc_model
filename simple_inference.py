import torch

import src.datasets.data as data
import src.utils.conf as conf
import src.utils.devices as devices
import src.runners.inference as inference

import os
from datetime import datetime
import sys
import shutil


def run_inference(model_path, data_path, beam_k=None, output_path=None):
    loaded_model = torch.load(
        model_path
    )

    config = loaded_model["config"]
    model = loaded_model["model"]
    env = conf.EnvConfiguration.from_env()
    device = devices.device_from_env(env)

    word_embeddings, dictionary = data.resolve_embeddings_and_dictionary(
        data_vocab_path=env.data_dict[config.dataset_name]["vocab_path"],
        max_vocab=config.max_vocab,
        vector_cache_path=env.vector_cache_path,
        vector_file_name=config.vector_file_name,
        device=device,
        num_oov=config.num_oov,
        verbose=False
    )
    word_embeddings.learned_embeddings = model.learned_embeddings
    corpus = data.resolve_corpus(
        data_path=data_path,
        max_sentence_length=80,
    )
    inf = inference.Inference(
        model=model, dictionary=dictionary,
        word_embeddings=word_embeddings, device=device,
        config=config, beam_k=beam_k,
    )

    if output_path is None:
        def write(string):
            print(string)
    else:
        f = open(output_path, "w")
        def write(string):
            f.write(string + "\n")

    for translations, all_logprobs, sent_batch in \
            inf.corpus_inference(corpus, lambda _: _//2 + 1, batch_size=16):
        oov_dicts = dictionary.get_oov_dicts(sent_batch)
        for line in dictionary.ids2sentences(
                translations, oov_dicts, oov_fallback=True):
            write(line)

def run_inference_with_compression(model_path, data_path, beam_k=None, output_path=None, compression=2):
    loaded_model = torch.load(
        model_path
    )

    config = loaded_model["config"]
    model = loaded_model["model"]
    env = conf.EnvConfiguration.from_env()
    device = devices.device_from_env(env)

    word_embeddings, dictionary = data.resolve_embeddings_and_dictionary(
        data_vocab_path=env.data_dict[config.dataset_name]["vocab_path"],
        max_vocab=config.max_vocab,
        vector_cache_path=env.vector_cache_path,
        vector_file_name=config.vector_file_name,
        device=device,
        num_oov=config.num_oov,
        verbose=False
    )
    word_embeddings.learned_embeddings = model.learned_embeddings
    corpus = data.resolve_corpus(
        data_path=data_path,
        max_sentence_length=80,
    )
    inf = inference.Inference(
        model=model, dictionary=dictionary,
        word_embeddings=word_embeddings, device=device,
        config=config, beam_k=beam_k,
    )

    if output_path is None:
        def write(string):
            print(string)
    else:
        f = open(output_path, "w")
        def write(string):
            f.write(string + "\n")

    for translations, all_logprobs, sent_batch in \
            inf.corpus_inference(corpus, lambda _: _//compression + 1, batch_size=16):
        oov_dicts = dictionary.get_oov_dicts(sent_batch)
        for line in dictionary.ids2sentences(
                translations, oov_dicts, oov_fallback=True):
            write(line)

def dae_compress(model_path, inputs_folder, outputs_folder, compression):
    count = 0
    start_datetime = datetime.now()

    if os.path.isdir(outputs_folder):
        shutil.rmtree(outputs_folder)
    os.mkdir(outputs_folder)

    for filename in os.listdir(inputs_folder):
        input_path = os.path.join(inputs_folder, filename)
        out_path = os.path.join(outputs_folder, filename)
        print('Processing DAE file: {}'.format(input_path))
        
        run_inference_with_compression(
            model_path=model_path,
            data_path=input_path,
            beam_k=None,
            output_path=out_path,
            compression=compression
        )
        count += 1
        print('Processed :{} file(s)'.format(count))
        # current_dateime = datetime.now()
        # diff_datetime = current_dateime - start_datetime
        # print("Run for: {}".format(diff_datetime))

# for a file
# if __name__ == "__main__":
#     import argparse
#     parser = argparse.ArgumentParser(description='Inference')
#     parser.add_argument('--model-path')
#     parser.add_argument('--data-path')
#     parser.add_argument('--beam-k', default=None, type=int)
#     parser.add_argument('--output-path', default=None)
#     args = parser.parse_args()

#     run_inference(
#         model_path=args.model_path,
#         data_path=args.data_path,
#         beam_k=args.beam_k,
#         output_path=args.output_path,
#     )

# for all file folder
if __name__ == "__main__":

    if len(sys.argv) != 5:
        print('python file.py [model] [inputs] [outputs] [compress]')
        sys.exit()

    # model_path = './_data/models/pc_4_lstm_nli_2g_2019.05.08.13.40.04.263_180000.p'
    # inputs_folder = './java/src/textrank/_data/en_outputs_6_sent_per_doc'
    # outputs_folder = './_data/textrank_6_sents_compress_2'

    model_path = sys.argv[1]
    inputs_folder = sys.argv[2]
    outputs_folder = sys.argv[3]
    compress_ratio = float(sys.argv[4])

    count = 0
    start_datetime = datetime.now()

    if os.path.isdir(outputs_folder):
        shutil.rmtree(outputs_folder)
    os.mkdir(outputs_folder)

    for filename in os.listdir(inputs_folder):
        input_path = inputs_folder + '/' + filename
        out_path = outputs_folder + '/' + filename
        print(input_path)
        count += 1
        print('files count: {}'.format(count))
        run_inference_with_compression(
            model_path=model_path,
            data_path=input_path,
            beam_k=None,
            output_path=out_path,
            compression=compress_ratio
        )
        current_dateime = datetime.now()
        diff_datetime = current_dateime - start_datetime
        print("Run for: {}".format(diff_datetime))

"""
python sample_scripts/simple_inference.py \
  --model-path {model_path} \
  --data-path {data_path}
"""
