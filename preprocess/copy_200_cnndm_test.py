import shutil
import os

INPUT_FOLDER_ORIGON = '__data/cnndm_test/origins'
OUT_FOLDER_ORIGIN = '__data/cnndm_test_abstractive/origins'

INPUT_FOLDER_REF = '__data/cnndm_test/references'
OUT_FOLDER_REF = '__data/cnndm_test_abstractive/refs'

def copy():
    count = 0
    accepted_file = 0
    for filename in os.listdir(INPUT_FOLDER_ORIGON):
        count_avg = get_avg_word(INPUT_FOLDER_ORIGON + '/' + filename)
        print('count avg = {}'.format(count_avg))
        if count_avg > 30:
            shutil.copyfile(INPUT_FOLDER_ORIGON + '/' + filename, OUT_FOLDER_ORIGIN + '/' + filename)
            shutil.copyfile(INPUT_FOLDER_REF + '/' + filename, OUT_FOLDER_REF + '/' + filename)
            accepted_file += 1
            
        count += 1
        print('count = {}; accepted file: {}'.format(count, accepted_file))

def get_avg_word(in_file):
    f = open(in_file, 'r')
    word_count = 0
    line_count = 0
    for s in f:
        line_count += 1
        word_count += len(s.split(' '))
    return word_count // line_count


if __name__ == '__main__':
    copy()