import os
import sys
import shutil

CNN_PARAM_IN_FOLDER = '__data/neuralsum/neuralsum/cnn/test'
CNN_PARAM_OUT_ORIGIN_FOLDER = '__data/cnn_test_extractive/origins'
CNN_PARAM_OUT_REF_FOLDER = '__data/cnn_test_extractive/refs'

DM_PARAM_IN_FOLDER = '__data/neuralsum/neuralsum/dailymail/test'
DM_PARAM_OUT_ORIGIN_FOLDER = '__data/dm_test_extractive/origins'
DM_PARAM_OUT_REF_FOLDER = '__data/dm_test_extractive/refs'

def extract_labeled_doc_to_origins_refs(in_folder, out_origin_folder, out_ref_folder):
    processed_file = 0
    for filename in os.listdir(in_folder):
        try:
            f = open (os.path.join(in_folder, filename), 'r')
            doc = f.read()
            f.close()
            doc = doc.split("\n\n")[1]
            sents = []
            labels = []
            lines = doc.split("\n")
            for line in lines:
                sent_label = line.split("\t\t\t")
                if len(sent_label) == 2:
                    sents.append(sent_label[0])
                    labels.append(sent_label[1])
            write_sents_to_origin(sents, out_origin_folder, filename)
            write_sents_to_ref(sents, labels, out_ref_folder, filename)
            processed_file += 1
            print('Processed: {} files'.format(processed_file))
        except:
            print('Ignore file: {}'.format(filename))
        if processed_file == 200:
            break

def write_sents_to_origin(sents, out_folder, filename):
    filename = filename.split('.')[0]
    doc = ''
    for sent in sents:
        doc += sent + "\n"
    f = open(os.path.join(out_folder, filename), 'w')
    f.write(doc)
    f.close()
    print('Wrote to {} in {}'.format(filename, out_folder))

def write_sents_to_ref(sents, labels, out_folder, filename):
    filename = filename.split('.')[0]
    count = 0
    doc = ''
    for count in range(len(labels)):
        if labels[count] == '1':
            doc += sents[count] + "\n"
    f = open(os.path.join(out_folder, filename), 'w')
    f.write(doc)
    f.close()
    print('Wrote to {} in {}'.format(filename, out_folder))


if __name__ == '__main__':
    extract_labeled_doc_to_origins_refs(CNN_PARAM_IN_FOLDER, CNN_PARAM_OUT_ORIGIN_FOLDER, CNN_PARAM_OUT_REF_FOLDER)