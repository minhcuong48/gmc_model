from sf.sfsinglesummarizer import SfSingleSummarizer
import os
import sys
import shutil

def sf(in_folder, out_folder, compression):
    if os.path.isdir(out_folder): shutil.rmtree(out_folder)
    os.mkdir(out_folder)

    count = 0
    for filename in os.listdir(in_folder):
        in_file = os.path.join(in_folder, filename)
        out_file = os.path.join(out_folder, filename)
        print('Processing SF file: {}'.format(in_file))
        summarizer = SfSingleSummarizer(in_file, out_file)
        summarizer.summarize(compression)
        count += 1
        print('Processed SF: {} file(s)'.format(count))

def sf_by_word_count(in_folder, out_folder, word_count):
    if os.path.isdir(out_folder): shutil.rmtree(out_folder)
    os.mkdir(out_folder)

    count = 0
    for filename in os.listdir(in_folder):
        summarizer = SfSingleSummarizer(in_folder + '/' + filename, out_folder + '/' + filename)
        summarizer.summarize_by_word_count(word_count)
        count += 1
        print('Processed: {} ({})'.format(filename, count))


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('python3.7 file.py [in_folder] [out_folder] [compression]')
        sys.exit()

    sf(sys.argv[1], sys.argv[2], float(sys.argv[3]))

    