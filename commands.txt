# create vocab from traning data
python src/datasets/preprocess.py [training_data] [out_vocab]

# run training
python dae_json.py runs/default/en.json
python dae_json.py runs/default/vi.json

# model names
en_180k
vi_xxxk

# run test
python simple_inference.py --model-path [model_path] --data-path [input.txt] --output-path [out.txt]
