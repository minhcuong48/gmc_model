import time
from my_utils.dataset_analysis import get_avg_word_count_per_doc
from simple_inference import dae_compress
from simple_inference import run_inference_with_compression
from my_utils.rouge_folder import rouge_folder
import sys
import os
import subprocess
import json


def dae_textrank(input_folder, ref_folder, root_folder, model_path, compress_dae, num_word_final):
    started_time = time.time()
    started_time_str = str(started_time)

    root_folder += '/' + started_time_str + '_dae_textrank_' + str(compress_dae) + '_' + str(num_word_final)
    print('ROOT FOLDER:')
    print(root_folder)
    textrank_folder = root_folder + '/' + 'textrank'
    dae_folder = root_folder + '/' + 'dae'
    textrank_folder_compress_final = root_folder + '/' + 'textrank_final'

    os.mkdir(root_folder)
    os.mkdir(textrank_folder)
    os.mkdir(dae_folder)
    os.mkdir(textrank_folder_compress_final)

    # begin
    count = 0
    for filename in os.listdir(input_folder):
        input_path = input_folder + '/' + filename
        out_path = dae_folder + '/' + filename
        print(input_path)
        count += 1
        print('files count: {}'.format(count))
        run_inference_with_compression(
            model_path=model_path,
            data_path=input_path,
            beam_k=None,
            output_path=out_path,
            compression=compress_dae
        )
        print("Processed by DAE: {} {} file(s)".format(filename, count))

        # textrank
        subprocess.call(['java', '-jar', 'textrank_single_file.jar', dae_folder + '/' + filename, textrank_folder + '/' + filename, str(num_word_final)])
        print('Processed by TextRank: {} ({})'.format(filename, count))

        # textrank_final
        # textrank compress final
        
        subprocess.call(['java', '-jar', 'textrank_single_file.jar', input_folder + '/' + filename, textrank_folder_compress_final + '/' + filename, str(num_word_final)])
    # end
    if ref_folder != None:
        rouge_folder(textrank_folder, ref_folder, root_folder + '/rouge_dae_textrank_final.txt')
        rouge_folder(textrank_folder_compress_final, ref_folder, root_folder + '/rouge_textrank_final.txt')

if __name__ == '__main__':
    f = open('setting_dae_textrank.json', 'r')
    setting = json.load(f)
    f.close()

    # param
    in_folder = setting['in_folder']
    ref_folder = setting['ref_folder']
    root_folder = setting['root_folder']
    model_path = setting['model_path']
    compress_x = setting['compress_x']
    num_of_words = setting['num_of_words']

    dae_textrank(in_folder, ref_folder, root_folder, model_path, compress_x, num_of_words)
    
