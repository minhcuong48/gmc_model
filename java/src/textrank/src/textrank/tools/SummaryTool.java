package textrank.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cuong.preprocessors.parsers.SentenceParser;
import com.cuong.preprocessors.parsers.TokenParser;

import ai.vitk.type.Token;
import textrank.comparators.SentenceComparator;
import textrank.comparators.SentenceComparatorForSummary;
import textrank.models.Paragraph;
import textrank.models.Segment;
import textrank.models.Sentence;
import textrank.readers.SegmentedDocumentReader;
import textrank.utils.TextUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MC48
 */
public class SummaryTool {
	private String fileNameIn;
	private String fileNameOut;

	static double d = 0.85;

	FileInputStream in;
	FileOutputStream out;
	List<Sentence> sentences, contentSummary;
	List<Paragraph> paragraphs;
	int noOfSentences, noOfParagraphs;

	double[][] intersectionMatrix;
	LinkedHashMap<Sentence, Double> dictionary;

	public SummaryTool() {
		in = null;
		out = null;
		noOfSentences = 0;
		noOfParagraphs = 0;
		this.fileNameIn = null;
		this.fileNameOut = null;
	}

	SummaryTool(String fileNameIn, String fileNameOut) {
		in = null;
		out = null;
		noOfSentences = 0;
		noOfParagraphs = 0;
		this.fileNameIn = fileNameIn;
		this.fileNameOut = fileNameOut;
	}

	void init() {
		sentences = new ArrayList<Sentence>();
		paragraphs = new ArrayList<Paragraph>();
		contentSummary = new ArrayList<Sentence>();
		dictionary = new LinkedHashMap<Sentence, Double>();
		noOfSentences = 0;
		noOfParagraphs = 0;
		try {
			if (fileNameIn != null) {
				in = new FileInputStream(fileNameIn);
			}
			if (fileNameOut != null) {
				// check if file exist then delete file
				File file = new File(fileNameOut);
				if (file.exists()) {
					file.delete();
				}
				out = new FileOutputStream(fileNameOut);
//				System.out.println("Created out: " + out.toString());
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/* Gets the sentences from the entire passage */
//	void extractSentenceFromContext(){
//		int nextChar,j=0;
//		int prevChar = -1;
//        try{
//	        while((nextChar = in.read()) != -1) {
//				j=0;
//	        	char[] temp = new char[100000];
//	        	while((char)nextChar != '.'){
//	        		//System.out.println(nextChar + " ");
//	        		temp[j] = (char)nextChar;
//	        		if((nextChar = in.read()) == -1){
//	        			break;
//	        		}
//	        		if((char)nextChar == '\n' && (char)prevChar == '\n'){
//	        			noOfParagraphs++;
//	        		}
//	        		j++;
//	        		prevChar = nextChar;
//	        	}
//                        Sentence sentence = new Sentence(noOfSentences,(new String(temp)).trim(),(new String(temp)).trim().length(),noOfParagraphs);
//	        	sentence.score = 1;
//                        dictionary.put(sentence, sentence.score);
//                        sentences.add(sentence);
//	        	noOfSentences++;
//	        	prevChar = nextChar;
//	        }
//	    }catch(Exception e){
//	    	e.printStackTrace();
//	    }
//
//	}

	void extractSentenceFromContext() {
		StringBuffer stringBuffer = new StringBuffer();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileNameIn)));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");
			}
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException ex) {
			ex.getMessage();
		}
		String entireText = stringBuffer.toString().trim();
		String[] arrayParagraphs = entireText.split("\n");
		for (int i = 0; i < arrayParagraphs.length; i++) {
			String paragraph = arrayParagraphs[i].trim();
			if (paragraph.isEmpty()) {
				continue;
			}
			extractSentencesAndTokens(paragraph, i);
			noOfParagraphs++;
		}
	}

	void extractSentencesFromContextDAE() {
		StringBuffer stringBuffer = new StringBuffer();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileNameIn)));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");
			}
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException ex) {
			ex.getMessage();
		}
		String entireText = stringBuffer.toString().trim();
		String[] arrayParagraphs = new String[1];
		arrayParagraphs[0] = entireText;
		extractSentencesAndTokensDAE(arrayParagraphs[0], 0);
		noOfParagraphs++;
	}

	// split document string to sentences and tokens
	void extractSentencesFromDocumentString(String document) {
		String entireText = document.trim();
		String[] arrayParagraphs = entireText.split("\n");
		for (int i = 0; i < arrayParagraphs.length; i++) {
			String paragraph = arrayParagraphs[i].trim();
			if (paragraph.isEmpty()) {
				continue;
			}
			extractSentencesAndTokens(paragraph, i);
			noOfParagraphs++;
		}
	}

	// handling for paragraph using vitk and vbee tokenizer
	void extractSentencesAndTokens(String paragraph, int paragraphIndex) {
		SentenceParser sentenceParser = new SentenceParser();
		TokenParser tokenParser = new TokenParser();

		List<String> sentenceStrings = sentenceParser.extract(paragraph);
		for (String s : sentenceStrings) {
			s = s.trim();

			List<Token> tokens = tokenParser.parse(s);

			Sentence sentence = new Sentence(noOfSentences, s, s.length(), paragraphIndex);
			sentence.setTokens(tokens);
			sentences.add(sentence);
			// System.out.println(sentence.value);
			sentence.score = 0.5;
			dictionary.put(sentence, sentence.score);
			noOfSentences++;
		}
	}

	// this method is for using DAE later. sentences are splitted by "\n" and there
	// is only one paragraph
	void extractSentencesAndTokensDAE(String paragraph, int paragraphIndex) {
		TokenParser tokenParser = new TokenParser();

		String[] tmp_sentences = paragraph.split("\n");

		for (String s : tmp_sentences) {
			s = s.trim();

			List<Token> tokens = tokenParser.parse(s);

			Sentence sentence = new Sentence(noOfSentences, s, s.length(), paragraphIndex);
			sentence.setTokens(tokens);
			sentences.add(sentence);
			// System.out.println(sentence.value);
			sentence.score = 0.5;
			dictionary.put(sentence, sentence.score);
			noOfSentences++;
		}
	}

	// extract sentences from segmented source (using Word Tokenizer by le hong
	// phuong)
	void extractSentenceFromStemmedSource() {
		SegmentedDocumentReader segmentedDocumentReader = new SegmentedDocumentReader(fileNameIn);
		segmentedDocumentReader.extract();

		paragraphs = segmentedDocumentReader.paragraphs;
		noOfParagraphs = paragraphs.size();

		// collect sentences from paragraphs to sentences
		for (Paragraph p : paragraphs) {
			for (Sentence s : p.sentences) {
				sentences.add(s);
				s.score = 1;
				dictionary.put(s, s.score);
			}
		}

		noOfSentences = sentences.size();
	}

	void groupSentencesIntoParagraphs() {
		int paraNum = 0;
		Paragraph paragraph = new Paragraph(0);

		for (int i = 0; i < noOfSentences; i++) {
			if (sentences.get(i).paragraphNumber == paraNum) {
				// continue
			} else {
				paragraphs.add(paragraph);
				paraNum++;
				paragraph = new Paragraph(paraNum);

			}
			paragraph.sentences.add(sentences.get(i));
		}

		paragraphs.add(paragraph);
	}

	double noOfCommonWords(Sentence str1, Sentence str2) {
		double commonCount = 0;

		for (String str1Word : str1.value.split("\\s+")) {
			for (String str2Word : str2.value.split("\\s+")) {
				if (str1Word.compareToIgnoreCase(str2Word) == 0) {
					commonCount++;
				}
			}
		}

		return commonCount;
	}

	double noOfCommonWordsUsingToken(Sentence s1, Sentence s2) {

		List<Segment> commonSegments = new ArrayList<>();

		List<Segment> segments1 = new ArrayList<>();
		for (Segment segment : s1.segments) {
			if (segment.isWord()) {
				segments1.add(segment);
			}
		}

		List<Segment> segments2 = new ArrayList<>();
		for (Segment segment : s2.segments) {
			if (segment.isWord()) {
				segments2.add(segment);
			}
		}

		for (Segment segment1 : segments1) {
			for (Segment segment2 : segments2) {
				if (segment1.equals(segment2)) {
					commonSegments.add(segment1);
				}
			}
		}

		return commonSegments.size();
	}

	double noOfCommonTokens(Sentence s1, Sentence s2) {
		Set<Token> commonTokens = new HashSet<>();

		List<Token> sentence1WordTokens = new ArrayList<>();
		List<Token> sentence2WordTokens = new ArrayList<>();

		for (Token t : s1.tokens) {
			if (isWord(t)) {
				sentence1WordTokens.add(t);
			}
		}

		for (Token t : s2.tokens) {
			if (isWord(t)) {
				sentence2WordTokens.add(t);
			}
		}

		for (Token t1 : sentence1WordTokens) {
			for (Token t2 : sentence2WordTokens) {
				if (t1.getWord().equals(t2.getWord()) && t1.getLemma().equals(t2.getLemma())) {
					commonTokens.add(t1);
				}
			}
		}

		return commonTokens.size();
	}

	boolean isWord(Token t) {
		if (t.getLemma().equals("WORD")) {
			return true;
		}
		return false;
	}

	double weight(Sentence s1, Sentence s2) {
		if (noOfCommonTokens(s1, s2) == 0) {
			return 0.0;
		}
		double result = noOfCommonTokens(s1, s2) / (double) (Math.log10(s1.noOfWords) + Math.log10(s2.noOfWords));
		if (Double.isNaN(result)) {
			result = 0.0;
		}
		if (Double.isInfinite(result)) {
			result = 0.0;
		}
		return result;
	}

	void createIntersectionMatrix() {
		intersectionMatrix = new double[noOfSentences][noOfSentences];
		for (int i = 0; i < noOfSentences; i++) {
			for (int j = 0; j < noOfSentences; j++) {

				if (i <= j) {
					Sentence str1 = sentences.get(i);
					Sentence str2 = sentences.get(j);
					// calculate similarity degree as weight
					intersectionMatrix[i][j] = noOfCommonWords(str1, str2)
							/ ((double) (str1.value.split("\\s+").length + str2.value.split("\\s+").length) / 2);
//					intersectionMatrix[i][j] = weight(str1, str2);
				} else {
					intersectionMatrix[i][j] = intersectionMatrix[j][i];
				}

			}
		}
	}

	// tong trong so tat ca cac canh xuat phat tu dinh i
	double totalEdgeWeightFromVertex(int i) {
		double total = 0.0;
		for (int j = 0; j < noOfSentences; j++) {
			// ignore weight from a vertex to itself
			if (i == j) {
				continue;
			}
			// add weight from vertex i to an other vertex of out(i)
			total += intersectionMatrix[i][j];
		}
		return total;
	}

	// cong thuc tinh trong so: Tong(t)
	double totalVertexIn(int i) {
		double total = 0.0;
		for (int j = 0; j < noOfSentences; j++) {
			if (i == j) {
				continue;
			}

			Sentence sentenceJ = sentences.get(j);
			double scoreJ = dictionary.get(sentenceJ);
			// System.out.println("scoreJ = " + scoreJ);
//			if (totalEdgeWeightFromVertex(j) == 0.0) {
//				System.out.println("totalEdgeWeightFromVertex: " + totalEdgeWeightFromVertex(j));
//			}
			// System.out.println("intersec[][]: " + intersectionMatrix[i][j]);
			double totalEdgeWeight = totalEdgeWeightFromVertex(j);
			if (totalEdgeWeight == 0.0) {
				totalEdgeWeight = 1;
			}
			total += intersectionMatrix[i][j] * scoreJ / totalEdgeWeight;

		}
//		System.out.println("totalVertexIn = " + total);
		return total;
	}

	void createDictionary() {

		for (int k = 0; k < 200; k++) {
			for (int i = 0; i < noOfSentences; i++) {
				double score = (1 - d) + d * totalVertexIn(i);
//				System.out.println("score: " + score);
				dictionary.put(sentences.get(i), score);
				((Sentence) sentences.get(i)).score = score;
			}
		}
	}

	// create summary with multiple paragraphs
	void createSummary(int ratio) {
		contentSummary.clear();

		for (int j = 0; j < noOfParagraphs; j++) {
			int primary_set = paragraphs.get(j).sentences.size() / ratio;

			// Sort based on score (importance)
			Collections.sort(paragraphs.get(j).sentences, new SentenceComparator());
			for (int i = 0; i < primary_set; i++) {
				contentSummary.add(paragraphs.get(j).sentences.get(i));
			}
		}

		// To ensure proper ordering
		Collections.sort(contentSummary, new SentenceComparatorForSummary());
	}

	// create summary with only one paragraphs
	void createSummaryOneParagraph(int pivot) {
		// System.out.println("Pivot IN = " + pivot);
		// System.out.println("CREATE SUMMARY ONE PARAGRAPH");
		contentSummary.clear();
		Collections.sort(sentences, new SentenceComparator());

		int currentWordCount = 0;
		for (Sentence sentence : sentences) {
			currentWordCount += sentence.value.split("\\s+").length;
			
			if(currentWordCount > pivot) {
				break;
			}
			contentSummary.add(sentence);
		}
		if(contentSummary.size() <= 0) {
			contentSummary.add(sentences.get(0));
		}
	}

	int numberOfWord() {
		int count = 0;
		for (Sentence s : sentences) {
			count += s.value.split("\\s+").length;
		}
		return count;
	}

	void createSummaryCompress(double compression) {
		int numWord = (int)((double)numberOfWord() / compression);
		createSummaryOneParagraph(numWord);
	}

	// create summary with only one paragraphs
	void createSummaryOneParagraphBaseSentNum(int sent_num) {
		// System.out.println("Pivot IN = " + pivot);
		// System.out.println("CREATE SUMMARY ONE PARAGRAPH");
		contentSummary.clear();
		Collections.sort(sentences, new SentenceComparator());

		int current_sent_num = 0;
		for (Sentence sentence : sentences) {
			contentSummary.add(sentence);
			current_sent_num += 1;
			if (current_sent_num >= sent_num) {
				break;
			}
		}

		// To ensure summary always has content
		if (contentSummary.isEmpty()) {
			contentSummary.add(sentences.get(0));
		}

		// To ensure proper ordering
		Collections.sort(contentSummary, new SentenceComparatorForSummary());
	}

	void printSentences() {
		for (Sentence sentence : sentences) {
			System.out.println(sentence.number + " => " + sentence.value + " => " + sentence.stringLength + " => "
					+ sentence.noOfWords + " => " + sentence.paragraphNumber);
		}
	}

	void printIntersectionMatrix() {
		for (int i = 0; i < noOfSentences; i++) {
			for (int j = 0; j < noOfSentences; j++) {
				System.out.print(intersectionMatrix[i][j] + "    ");
			}
			System.out.print("\n");
		}
	}

	@SuppressWarnings("rawtypes")
	void printDicationary() {
		System.out.println("DICTONARYYYYYYYYYYYYYY");
//		   Get a set of the entries
		Set set = dictionary.entrySet();
		// Get an iterator
		Iterator i = set.iterator();
		// Display elements
		while (i.hasNext()) {
			Map.Entry me = (Map.Entry) i.next();
			System.out.println(((Sentence) me.getKey()).number + ": " + " => score: " + me.getValue());
			// System.out.println(me.getValue());
		}
	}

	void printSummary() {
//		System.out.println("no of paragraphs = " + noOfParagraphs);
//		System.out.print("No of sentences: " + sentences.size());
//		System.out.println("No of content summary: " + contentSummary.size());
		StringBuilder builder = new StringBuilder();
		for (Sentence sentence : contentSummary) {
			builder.append(sentence.value);
			builder.append("\n");
		}

		try {
			out.write(builder.toString().trim().getBytes());
		} catch (IOException ex) {
			System.out.println("Error to write sentence: " + ex.getMessage());
		}

		try {
			out.close();
		} catch (IOException ex) {
			System.out.println("Closing output stream is failed");
		}
	}

	String getSummary() {
		StringBuilder builder = new StringBuilder();
		for (Sentence sentence : contentSummary) {
			builder.append(sentence.value);
			builder.append("\n");
		}
		return builder.toString().trim();
	}

	double getWordCount(List<Sentence> sentenceList) {
		double wordCount = 0.0;
		for (Sentence sentence : sentenceList) {
			wordCount += (sentence.value.split(" ")).length;
		}
		return wordCount;
	}

	void printStats() {
		System.out.println("number of words in Context : " + getWordCount(sentences));
		System.out.println("number of words in Summary : " + getWordCount(contentSummary));
		System.out.println("Commpression : " + getWordCount(contentSummary) / getWordCount(sentences));
	}

	void printSentenceOrderByScore() {
		System.out.println("RANKINGGGGGGGGG FOR ALL SENTENCES");
		Collections.sort(sentences, new SentenceComparator());
		for (Sentence sentence : sentences) {
			System.out.print("R-" + sentences.indexOf(sentence) + ": ");
			System.out.println(sentence.score + ": " + sentence.value);
		}
	}

	public void printContentSummary() {
		System.out.println("CONTENT SUMMARY");
		System.out.println("Line count of content summary: " + contentSummary.size());
		int wordCount = 0;
		for (Sentence sentence : contentSummary) {
			System.out.printf("#%-4d: W%-4d: R-%8f: %s%n", sentences.indexOf(sentence), sentence.number, sentence.score,
					sentence.value);
			wordCount += TextUtils.numberOfWords(sentence.value);
		}
		System.out.println("Word count of content summary: " + wordCount);
	}

	public static SummaryTool textRankForDAE(String fileNameIn, String fileNameOut, double compression) {
		SummaryTool summary = new SummaryTool(fileNameIn, fileNameOut);
		summary.init();
		summary.extractSentencesFromContextDAE();
		// summary.extractSentenceFromStemmedSource();
		summary.groupSentencesIntoParagraphs();
//		summary.printSentences();
		summary.createIntersectionMatrix();

//		System.out.println("INTERSECTION MATRIX");
//		summary.printIntersectionMatrix();

		summary.createDictionary();
//		summary.printDicationary();

//		System.out.println("SUMMMARY");
		summary.createSummaryCompress(compression);
		summary.printSummary();

		return summary;
	}
	
	public static SummaryTool textRankForDAEByWordCount(String fileNameIn, String fileNameOut, int num_of_words) {
		SummaryTool summary = new SummaryTool(fileNameIn, fileNameOut);
		summary.init();
		summary.extractSentencesFromContextDAE();
		// summary.extractSentenceFromStemmedSource();
		summary.groupSentencesIntoParagraphs();
//		summary.printSentences();
		summary.createIntersectionMatrix();

//		System.out.println("INTERSECTION MATRIX");
//		summary.printIntersectionMatrix();

		summary.createDictionary();
//		summary.printDicationary();

//		System.out.println("SUMMMARY");
		summary.createSummaryOneParagraph(num_of_words);
		summary.printSummary();

		return summary;
	}

}
