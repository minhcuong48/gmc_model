package textrank.utils;

import org.apache.commons.lang3.StringUtils;

public class TextUtils {

	public static int numberOfWords(String text) {
		int number = 0;

		for (int i = 0; i < text.length(); i++) {
			if (StringUtils.isWhitespace(String.valueOf(text.charAt(i)))) {
				number++;
			}
		}

		return number + 1;
	}

}
