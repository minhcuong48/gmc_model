package textrank.models;

import java.util.ArrayList;
import java.util.List;

import ai.vitk.type.Token;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MC48
 */
public class Sentence{
	
    public int paragraphNumber;
	public int number;
	public int stringLength; //Dont need this 
	public double score;
	public int noOfWords;
	public String value;
	public List<Segment> segments;
	public List<Token> tokens;

	public Sentence(int number, String value, int stringLength, int paragraphNumber){
		this.number = number;
		this.value = new String(value);
		this.stringLength = stringLength;
		noOfWords = 0;
		score = 0.0;
		this.paragraphNumber=paragraphNumber;
		this.segments = new ArrayList<>();
	}
	
	public Sentence(int number, int paragraphNumber) {
		this.number = number;
		this.paragraphNumber = paragraphNumber;
		this.segments = new ArrayList<>();
	}
	
	public void generateValueFromSegments() {
		this.value = "";
		for(Segment segment : segments) {
			if(segment.isWord()) {
				value += " " + segment.value;
			}
			else if(segment.isDot()) {
				value += segment.value;
			}
		}
		value = value.trim();
	}
	
	public void calculateNoOfWords() {
		int count = 0;
		for(Token t : tokens) {
			if(t.getLemma().equals("WORD")) {
				count++;
			}
		}
		this.noOfWords = count;
	}
	
	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
		calculateNoOfWords();
	}
	
	@Override
	public String toString() {
		String result = "\nParagraph Number: " + paragraphNumber
				+ "\nNumber: " + number
				+ "\nScore: " + score
				+ "\nNo of words: " + noOfWords
				+ "\nValue\n\t" + value
				+ "\nSegments: " + segments.size();
		
		for(Segment segment : segments) {
			result += "\n\t" + segment;
		}
		
		return result;
	}
	
}
