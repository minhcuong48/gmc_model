package textrank.models;

public class Segment {

	public String value;
	public String kind;
	public int index;	// index in para
	
	public Segment(String value, String kind, int index) {
		super();
		this.value = value.trim();
		this.kind = kind.trim();
		this.index = index;
	}
	
	public int getRhythmCount() {
		return value.split(" ").length;
	}
	
	public boolean isDot() {
		if(kind.equals("PUNCT") && value.equals(".")) {
			return true;
		}
		return false;
	}
	
	public boolean isWord() {
		if(kind.equals("WORD") || kind.equals("HYPHEN") 
				|| kind.equals("ALLCAP")
				|| kind.equals("CAPITAL")
				|| kind.equals("NAME")) {
			return true;
		}
		return false;
	}
	
	public boolean equals(Segment s) {
		if(this.kind.equals(s.kind) && this.value.equals(s.value)) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "#" + this.index + " | " + this.value + "(" + getRhythmCount() + ")| " + this.kind;
	}
	
	// TEST
	public static void main(String[] args) {
		Segment seg = new Segment(" Anh yeu oi ", "Word", 1);
		System.out.println(seg);
	}
}
