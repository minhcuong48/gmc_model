package textrank.models;

import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MC48
 */
public class Paragraph{
	
    public int number;
    public List<Sentence> sentences;
    public List<Segment> segments;

    public Paragraph(int number){
		this.number = number;
		this.sentences = new ArrayList<>();
		this.segments = new ArrayList<>();
    }

    public void generateSentencesFromSegments() {
    	int appendedSegmentsCount = 0;
    	int sentenceIndex = 0;
    	Sentence sentence = new Sentence(sentenceIndex++, number);
		for(Segment segment : segments) {
			// add segment to sentence
			sentence.segments.add(segment);
			// if end sentence then add sentence to list
			// and create new sentence
			if(segment.kind.equals("PUNCT") && segment.value.equals(".")) {
				sentences.add(sentence);
				sentence.generateValueFromSegments();
				sentence.calculateNoOfWords();
				appendedSegmentsCount += sentence.segments.size();
				sentence = new Sentence(sentenceIndex++, number);
			}
		}
		// if the sentence does not end by dot
		if(appendedSegmentsCount < segments.size()) {
			sentences.add(sentence);
			sentence.generateValueFromSegments();
			sentence.calculateNoOfWords();
		}
	}
    
    @Override
    public String toString() {
    	String result = "\nNumber: " + number
    			+ "\nSentences Size: " + sentences.size()
    			+ "\nSegments Size: " + segments.size();

    	return result;
    }
    
}
