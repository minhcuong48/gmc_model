package textrank.apps;

import textrank.tools.SummaryTool;

public class TextRank extends SummaryTool {
	
	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("java -jar file.jar [in_file] [out_file] [num_of_word]");
			System.exit(0);
		}
		double num_words = Double.parseDouble(args[2]);
		SummaryTool.textRankForDAEByWordCount(args[0], args[1], (int)(num_words));
		System.out.println("Processed: " + args[0]);
	}
	
}
