package textrank.apps;

import java.io.File;
import textrank.tools.SummaryTool;

public class FolderSummarizer {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("java -jar <jar_file> <in_folder> <out_folder> <compression>");
			System.exit(0);
		}
		FolderSummarizer summarizer = new FolderSummarizer();
		summarizer.sumarizeFolder(args[0], args[1], Double.parseDouble(args[2]));
	}

	public void sumarizeFolder(String inputFolderName, String outputFolderName, double compression) {
		checkOutputFolderExist(outputFolderName);
		File inputsFolder = new File(inputFolderName);
		File[] files = inputsFolder.listFiles();
		System.out.println("Number of files: " + files.length);
		int count = 0;
		for (File file : files) {
			// ignore the file that is not document. this is maybe system files
			if (file.getName().startsWith(".")) {
				continue;
			}

			SummaryTool.textRankForDAE(inputFolderName + "/" + file.getName(),
					outputFolderName + "/" + file.getName() + "_textrank", compression);
			count += 1;
			System.out.println("Processed: " + file.getName() + "(" + count + ")");
			// summaryTool.printContentSummary();

		}
		System.out.println("END");
	}

	public void checkOutputFolderExist(String output) {
		File file = new File(output);
		if (file.exists()) {
			file.delete();
		}
		file.mkdir();
	}

}
