package textrank.converters;

import java.io.File;
import java.text.Normalizer;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import textrank.readers.DocReader;

public class DocToText {

	public void convertAllFilesInFolder(String folderPath) {
		File inputsFolder = new File(folderPath);
		File[] files = inputsFolder.listFiles();
		System.out.println("Number of files: " + files.length);
		int index = 0;
		for (File file : files) {
			if (file.getName().startsWith(".")) {
				System.out.println("Ignore file: " + file.getName());
				continue;
			}
			index++;
			String fileName = String.format("%03d", index);
//			System.out.println("File: " + file.getName());
			convert("docs/" + file.getName(), "original-texts/" + fileName, "references-texts/" + fileName + "_ref");
		}
	}

	public String getStandardFileName(String fileName) {
		fileName = fileName.replaceAll(" ", "_");
		StringBuilder builder = new StringBuilder(fileName.toLowerCase());
		return normalizeString(builder.toString());
	}

	public String normalizeString(String original) {
		original = Normalizer.normalize(original, Normalizer.Form.NFD);
		original = original.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return original;
	}

	public void convert(String docFilePath, String originalTextPath, String summaryTextPath) {
		DocReader reader = new DocReader();
		String rawText = null;

		// check file extension is doc or docx to use right method to extract text
		File file = new File(docFilePath);
		if (FilenameUtils.getExtension(file.getName()).equals("doc")) {
			rawText = reader.readDocFile(docFilePath);
		} else if (FilenameUtils.getExtension(file.getName()).equals("docx")) {
			rawText = reader.readDocxFile(docFilePath);
		}

		rawText = preprocess(rawText);
		String originalText = getOriginalText(rawText);
		String summaryText = getSummaryText(rawText);
		reader.export(originalText, originalTextPath);
		reader.export(summaryText, summaryTextPath);
		System.out.println("Original Text");
		System.out.println(originalText);
		System.out.println("References Summary Text");
		System.out.println(summaryText);
	}

	public String preprocess(String rawText) {
		StringBuilder builder = new StringBuilder(rawText);

		// replace dot by - if dot is not end of sentence
		for (int i = 0; i < builder.length() - 1; i++) {
			if (builder.charAt(i) == '.' && !StringUtils.isWhitespace(String.format("%c", builder.charAt(i + 1)))
					&& builder.charAt(i + 1) != '.') {
				builder.setCharAt(i, '-');
			}
		}

		return builder.toString().trim();
	}

	public String getOriginalText(String rawText) {
		StringBuilder builder = new StringBuilder();

		int mark1 = 0;
		int mark2 = 0;

		String[] lines = rawText.split("\n");

		// get 2 marks begin original text and begin summary text
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (line.toLowerCase().contains("nguyên bản")) {
				mark1 = i;
			}
			if (line.toLowerCase().contains("tóm tắt")) {
				mark2 = i;
			}
		}

		// extract original text
		for (int i = mark1 + 1; i < mark2; i++) {
			if (lines[i].trim().isEmpty()) {
				continue;
			}
			builder.append(String.format("%n"));
			builder.append(lines[i]);
		}

		return builder.toString().trim();
	}

	public String getSummaryText(String rawText) {
		StringBuilder builder = new StringBuilder();

		int mark2 = 0;

		String[] lines = rawText.split("\n");

		// get 2 marks begin original text and begin summary text
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (line.toLowerCase().contains("tóm tắt")) {
				mark2 = i;
			}
		}

		// extract original text
		for (int i = mark2 + 1; i < lines.length; i++) {
			if (lines[i].trim().isEmpty()) {
				continue;
			}
			builder.append(String.format("%n"));
			builder.append(lines[i]);
		}

		return builder.toString().trim();
	}

	public static void main(String[] args) {
		DocToText docToText = new DocToText();
		docToText.convertAllFilesInFolder("docs");
		System.out.println("END");
	}

}
