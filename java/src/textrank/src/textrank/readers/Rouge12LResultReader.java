package textrank.readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Rouge12LResultReader {

	List<Result> lines = new ArrayList<>();
	double avgRecallRouge1 = 0.0;
	double avgPrecisionRouge1 = 0.0;
	double avgFScoreRouge1 = 0.0;

	double avgRecallRouge2 = 0.0;
	double avgPrecisionRouge2 = 0.0;
	double avgFScoreRouge2 = 0.0;

	double avgRecallRougeL = 0.0;
	double avgPrecisionRougeL = 0.0;
	double avgFScoreRougeL = 0.0;

	int totalFiles = 0;

	public void read(String inputPath, String outputPath) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(inputPath));

			// ignore first line
			if (scanner.hasNextLine()) {
				scanner.nextLine();
			}

			// loop line by line
			while (scanner.hasNext()) {
				String line = scanner.nextLine().trim();
				if (line.isEmpty()) {
					continue;
				}
				if (line.contains(".DS")) {
					continue;
				}
				System.out.println("line: " + line);
				String[] lineParts = line.split(",");

				// new result
				Result result = new Result();
				result.rougeType = lineParts[0];
				result.name = lineParts[1];
				result.method = lineParts[2];
				result.avgRecall = Double.valueOf(lineParts[3]);
				result.avgPrecision = Double.valueOf(lineParts[4]);
				result.avgFScore = Double.valueOf(lineParts[5]);

				lines.add(result);
			}

			// calc sum
			for (Result result : lines) {
				int index = lines.indexOf(result);
				switch (index % 3) {
				case 0:
					totalFiles++;
					avgRecallRouge1 += result.avgRecall;
					avgPrecisionRouge1 += result.avgPrecision;
					avgFScoreRouge1 += result.avgFScore;
					break;

				case 1:
					avgRecallRouge2 += result.avgRecall;
					avgPrecisionRouge2 += result.avgPrecision;
					avgFScoreRouge2 += result.avgFScore;
					break;

				case 2:
					avgRecallRougeL += result.avgRecall;
					avgPrecisionRougeL += result.avgPrecision;
					avgFScoreRougeL += result.avgFScore;
					break;
				}
			}

			// calc avg
			avgRecallRouge1 /= totalFiles;
			avgPrecisionRouge1 /= totalFiles;
			avgFScoreRouge1 /= totalFiles;

			avgRecallRouge2 /= totalFiles;
			avgPrecisionRouge2 /= totalFiles;
			avgFScoreRouge2 /= totalFiles;

			avgRecallRougeL /= totalFiles;
			avgPrecisionRougeL /= totalFiles;
			avgFScoreRougeL /= totalFiles;

			// write to file
			write(outputPath);
		} catch (FileNotFoundException e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			scanner.close();
		}

	}

	public void write(String outputPath) {
		File file = new File(outputPath);
		FileWriter out;
		try {
			out = new FileWriter(file);

			out.write(String.format("Total files: %d%n%n", totalFiles));

			out.write(String.format("ROUGE-1%n"));
			out.write(String.format("AVG RECALL: %f%n", avgRecallRouge1));
			out.write(String.format("AVG PRECISION: %f%n", avgPrecisionRouge1));
			out.write(String.format("AVG FSCORE: %f%n%n", avgFScoreRouge1));

			out.write(String.format("ROUGE-2%n"));
			out.write(String.format("AVG RECALL: %f%n", avgRecallRouge2));
			out.write(String.format("AVG PRECISION: %f%n", avgPrecisionRouge2));
			out.write(String.format("AVG FSCORE: %f%n%n", avgFScoreRouge2));

			out.write(String.format("ROUGE-L%n"));
			out.write(String.format("AVG RECALL: %f%n", avgRecallRougeL));
			out.write(String.format("AVG PRECISION: %f%n", avgPrecisionRougeL));
			out.write(String.format("AVG FSCORE: %f%n%n", avgFScoreRougeL));

			out.write(String.format("%-30s%-10s%-10s%-20s%-20s%-20s%n", "ROUGE", "NAME", "METHOD", "AVG_RECALL",
					"AVG_PRECISION", "AVG_FSCORE"));
			for (Result result : lines) {
				out.write(String.format("%-30s%-10s%-10s%-20f%-20f%-20f%n", result.rougeType, result.name,
						result.method, result.avgRecall, result.avgPrecision, result.avgFScore));
			}

			out.close();
		} catch (IOException e) {
			System.out.println("Exception: " + e.getMessage());
		}

	}

	public class Result {
		String rougeType;
		String name;
		String method;
		Double avgRecall;
		Double avgPrecision;
		Double avgFScore;

	}

	public static void main(String[] args) {
		Rouge12LResultReader reader = new Rouge12LResultReader();
		reader.read("results.csv", "results.txt");
	}

}
