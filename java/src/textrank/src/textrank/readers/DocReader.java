package textrank.readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class DocReader {

	public String readDocFile(String path) {
		String content = null;

		try {
			WordExtractor extractor = new WordExtractor(new FileInputStream(new File(path)));
			content = extractor.getText();
			extractor.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		return content;
	}

	public String readDocxFile(String path) {
		String content = null;

		try {
			XWPFDocument document = new XWPFDocument(new FileInputStream(new File(path)));
			XWPFWordExtractor extractor = new XWPFWordExtractor(document);
			content = extractor.getText();
			extractor.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		return content;
	}

	public void export(String content, String path) {
		try {
			FileWriter writer = new FileWriter(new File(path));

			writer.write(content);

			writer.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		System.out.println("START");
		DocReader reader = new DocReader();
		String docContent = reader.readDocFile("input.doc");
		System.out.println(docContent);
		System.out.println("END");
	}

}
