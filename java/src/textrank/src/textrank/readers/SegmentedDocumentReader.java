package textrank.readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

import textrank.models.Paragraph;
import textrank.models.Segment;
import textrank.models.Sentence;

public class SegmentedDocumentReader {

	private static Logger LOGGER = Logger.getLogger(SegmentedDocumentReader.class.getName());
	public String filePath;
	public List<Paragraph> paragraphs;
	public Set<String> kinds;
	
	public SegmentedDocumentReader(String filePath) {
		this.filePath = filePath;
	}
	
	public void extract() {
		// Init paragraphs and segments list
		init();
		
		try {
			// Create scanner to read document line by line
			Scanner scanner = new Scanner(new File(filePath));
			int paragraphIndex = 0;
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				// if line is empty then ignore
				if(line.isEmpty()) {
					continue;
				}
				
				// split to get segment (a segment includes raw value and kind)
				String[] segments = line.split(" ");
				
				// create indexes for counting
				int segmentIndex = 0;
				// Create paragraph
				Paragraph newParagraph = new Paragraph(paragraphIndex++);
				// loop in every segments
				for(String segment : segments) {
					segment = segment.trim();
					// split raw value and kind word
					String[] parts = segment.split("/");
					// if not enough info then ignore, maybe got some errors bcs of parser
					if(parts.length != 2) {
						continue;
					}
					// get kind of word
					String kind = parts[1];
					kinds.add(kind);
					// split rhythms
					String[] rhythms = parts[0].split("_");
					// init value for segment
					String value = "";
					// loop in every rhythms
					for(String rhythm : rhythms) {
						// concat to value of segment
						value += " " + rhythm;
					}
					// trim space for value
					value = value.trim();
					// create new segment
					Segment segmentObject = new Segment(value, kind, segmentIndex++);
					newParagraph.segments.add(segmentObject);
				}
				// generate sentences for new paragraph
				newParagraph.generateSentencesFromSegments();
				// add new paragraph to list
				paragraphs.add(newParagraph);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			LOGGER.severe(e.getMessage());
		}
	}
	
	public void printInfo() {
		System.out.println("Filepath: " + filePath);
		System.out.println("Paragraphs size: " + paragraphs.size());
		
		System.out.println("Kinds: " + kinds.size());
		for(String kind : kinds) {
			System.out.println("\t" + kind);
		}
		
		for(Paragraph p : paragraphs) {
			System.out.println(p);
		}
		
		for(Sentence sentence : paragraphs.get(6).sentences) {
			System.out.println(sentence);
		}
	}
	
	private void init() {
		paragraphs = new ArrayList<>();
		kinds = new HashSet<>();
	}
	
	// TEST
	public static void main(String[] args) {
		String filePath = "segmented_input/002";
		SegmentedDocumentReader segmentedDocumentReader = new SegmentedDocumentReader(filePath);
		segmentedDocumentReader.extract();
		segmentedDocumentReader.printInfo();
	}
	
}
