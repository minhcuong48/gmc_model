"""
Kich ban nay su dung de chay 5 phuong phap sau
voi ti le nen pha trich rut va toan cuc lan luot la: x, y
    1. DAE: y
    2. TextRank: y
    3. Sentence Features: y
    4. TextRank: x ; DAE: y/x'
    5. Sentence Features: x ; DAE: y/x'
Sau do thong ke lai so luong tokens trung binh cua moi method 
va rouge-1, rouge-l moi phuong phap
"""

import os
import sys
import time
import datetime
import json
import shutil
import subprocess
from simple_inference import dae_compress
from statistics import get_avg_words_per_doc_in_dir
from utils.rouge_filenames import get_rouge1L
from sf_folder import sf
from vi_preprocess import vi_preprocess
from en_preprocess import en_preprocess

def run_dae(model_path, in_dir, out_dir, compress):
    print('Running Sentence Compression with compress = {}'.format(compress))
    create_folder_force(out_dir)
    dae_compress(model_path, in_dir, out_dir, compress)

def run_textrank(in_dir, out_dir, compress):
    print('Running TextRank with compress: {}'.format(compress))
    create_folder_force(out_dir)
    subprocess.call(['java', '-jar', 'textrank_folder_compress.jar', in_dir, out_dir, str(compress)])

def run_sf(in_dir, out_dir, compress):
    print('Running Sentence Features with compress: {}'.format(compress))
    create_folder_force(out_dir)
    sf(in_dir, out_dir, compress)


def create_folder_force(path):
    if os.path.exists(path):
        shutil.rmtree(path)
        print('Deleted folder: {}'.format(path))
    os.mkdir(path)
    print('Created folder: {}'.format(path))

def calc_rouges(sys_dir, ref_dir):
    rouge1, rougel = get_rouge1L(sys_dir, ref_dir)
    print('Rouge for: {}'.format(sys_dir))
    print('Rouge-1 F: {}'.format(rouge1))
    print('Rouge-L F: {}'.format(rougel))
    return rouge1, rougel

def report(file, content, end='\n'):
    f = open(file, 'a')
    f.write(content)
    f.write(end)
    f.close()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('python file.py [setting_file]')
        sys.exit()

    # Doc setting file
    setting_file_path = sys.argv[1]
    print('Reading {} file ...'.format(setting_file_path))
    setting_file = open(setting_file_path, 'r')
    settings = json.load(setting_file)
    setting_file.close()

    # Trich xuat cac settings
    SET_NAME = settings['name']
    SET_PREPROCESS_MODE = settings['preprocess_mode']
    SET_EXTRACT_COMPRESS = settings['extract_compress']
    SET_TOTAL_COMPRESS = settings['total_compress']
    SET_ORIGINS_PATH = settings['origins_path']
    SET_REFS_PATH = settings['refs_path']
    SET_RESULTS_PATH = settings['results_path']
    SET_MODEL_PATH = settings['model_path']

    print('\tNAME = {}'.format(SET_NAME))
    print('\tPreprocess Mode = {}'.format(SET_PREPROCESS_MODE))
    print('\tExtract Compress = {}'.format(SET_EXTRACT_COMPRESS))
    print('\tTotal Compress = {}'.format(SET_TOTAL_COMPRESS))
    print('\tOrigins Path = {}'.format(SET_ORIGINS_PATH))
    print('\tRefs Path = {}'.format(SET_REFS_PATH))
    print('\tResults Path = {}'.format(SET_RESULTS_PATH))
    print('\tModel Path = {}'.format(SET_MODEL_PATH))
    
    # Tao root dir bang timestamps
    root_dir = str(datetime.datetime.now())
    root_dir = os.path.join(SET_RESULTS_PATH, root_dir + '_' + SET_NAME)
    create_folder_force(root_dir)

    # preprocess
    new_origins_path = os.path.join(root_dir, 'origins')
    new_refs_path = os.path.join(root_dir, 'refs')

    if SET_PREPROCESS_MODE != 'none':
        create_folder_force(new_origins_path)
        create_folder_force(new_refs_path)

    if SET_PREPROCESS_MODE == 'vi':
        vi_preprocess(SET_ORIGINS_PATH, new_origins_path)
        if len(SET_REFS_PATH) > 0:
            vi_preprocess(SET_REFS_PATH, new_refs_path)
    
    if SET_PREPROCESS_MODE == 'en':
        en_preprocess(SET_ORIGINS_PATH, new_origins_path)
        if len(SET_REFS_PATH) > 0:
            en_preprocess(SET_REFS_PATH, new_refs_path)
        
    if SET_PREPROCESS_MODE != 'none':
        SET_ORIGINS_PATH = new_origins_path
        if len(SET_REFS_PATH) > 0:
            SET_REFS_PATH = new_refs_path

    # Tinh so tu trung binh moi file cua origins va refs
    avg_words_per_doc_origins = get_avg_words_per_doc_in_dir(SET_ORIGINS_PATH)
    print('AVG words per docs in dir {} is {}'.format(SET_ORIGINS_PATH, avg_words_per_doc_origins))

    avg_words_per_doc_refs = 0
    if len(SET_REFS_PATH) > 0:
        avg_words_per_doc_refs = get_avg_words_per_doc_in_dir(SET_REFS_PATH)
        print('AVG words per docs in dir {} is {}'.format(SET_REFS_PATH, avg_words_per_doc_refs))

    

    # 1 - Chay textrank voi total_compress
    tr_path = os.path.join(root_dir, 'tr')
    run_textrank(SET_ORIGINS_PATH, tr_path, SET_TOTAL_COMPRESS)
    avg_words_per_doc_tr = get_avg_words_per_doc_in_dir(tr_path)
    print('AVG words per docs in dir {} is {}'.format(tr_path, avg_words_per_doc_tr))
    actual_total_compress_tr = float(avg_words_per_doc_origins) / float(avg_words_per_doc_tr)
    print('Actual compress of TextRank = {}'.format(actual_total_compress_tr))
    print('We will use this compress for DAE right now!')

    # 2 - Chay DAE voi actual_total_compress_tr
    dae_vs_tr_path = os.path.join(root_dir, 'dae_vs_tr')
    run_dae(SET_MODEL_PATH, SET_ORIGINS_PATH, dae_vs_tr_path, actual_total_compress_tr)
    avg_words_per_doc_dae_vs_tr = get_avg_words_per_doc_in_dir(dae_vs_tr_path)
    actual_total_compress_dae_vs_tr = float(avg_words_per_doc_origins) / float(avg_words_per_doc_dae_vs_tr)

    # 3a - Chay TextRank Extract voi extract compress
    tr_extract_path = os.path.join(root_dir, 'tr_extract')
    run_textrank(SET_ORIGINS_PATH, tr_extract_path, SET_EXTRACT_COMPRESS)
    avg_words_per_doc_tr_extract = get_avg_words_per_doc_in_dir(tr_extract_path)
    print('AVG words per docs in dir {} is {}'.format(tr_extract_path, avg_words_per_doc_tr_extract))
    actual_extract_compress_tr = float(avg_words_per_doc_origins) / float(avg_words_per_doc_tr_extract)
    print('Actual compress of TextRank in Extract Phrase = {}'.format(actual_extract_compress_tr))
    remain_compress_dae_tr = float(actual_total_compress_tr) / float(actual_extract_compress_tr)
    print('So compress in DAE phrase is: {}'.format(remain_compress_dae_tr))

    # 3b - Chay DAE Phrase sau TextRank
    tr_dae_dir_path = os.path.join(root_dir, 'tr_dae')
    run_dae(SET_MODEL_PATH, tr_extract_path, tr_dae_dir_path, remain_compress_dae_tr)
    avg_words_per_doc_tr_dae = get_avg_words_per_doc_in_dir(tr_dae_dir_path)
    actual_total_compress_tr_dae = float(avg_words_per_doc_origins) / float(avg_words_per_doc_tr_dae)

    # 4 - Chay Sentence Features voi total_compress
    sf_path = os.path.join(root_dir, 'sf')
    run_sf(SET_ORIGINS_PATH, sf_path, SET_TOTAL_COMPRESS)
    avg_words_per_doc_sf = get_avg_words_per_doc_in_dir(sf_path)
    print('AVG words per docs in dir {} is {}'.format(sf_path, avg_words_per_doc_sf))
    actual_total_compress_sf = float(avg_words_per_doc_origins) / float(avg_words_per_doc_sf)
    print('Actual compress of SF = {}'.format(actual_total_compress_sf))
    print('We will use this compress for DAE right now!')

    # 5 - Chay DAE voi actual_total_compress_sf
    dae_vs_sf_path = os.path.join(root_dir, 'dae_vs_sf')
    run_dae(SET_MODEL_PATH, SET_ORIGINS_PATH, dae_vs_sf_path, actual_total_compress_sf)
    avg_words_per_doc_dae_vs_sf = get_avg_words_per_doc_in_dir(dae_vs_sf_path)
    actual_total_compress_dae_vs_sf = float(avg_words_per_doc_origins) / float(avg_words_per_doc_dae_vs_sf)

    # 6a - Chay SF Extract voi extract compress
    sf_extract_path = os.path.join(root_dir, 'sf_extract')
    run_sf(SET_ORIGINS_PATH, sf_extract_path, SET_EXTRACT_COMPRESS)
    avg_words_per_doc_sf_extract = get_avg_words_per_doc_in_dir(sf_extract_path)
    print('AVG words per docs in dir {} is {}'.format(sf_extract_path, avg_words_per_doc_sf_extract))
    actual_extract_compress_sf = float(avg_words_per_doc_origins) / float(avg_words_per_doc_sf_extract)
    print('Actual compress of SF in Extract Phrase = {}'.format(actual_extract_compress_sf))
    remain_compress_dae_sf = float(actual_total_compress_sf) / float(actual_extract_compress_sf)
    print('So compress in DAE phrase is: {}'.format(remain_compress_dae_sf))

    # 3b - Chay DAE Phrase sau SF
    sf_dae_dir_path = os.path.join(root_dir, 'sf_dae')
    run_dae(SET_MODEL_PATH, sf_extract_path, sf_dae_dir_path, remain_compress_dae_sf)
    avg_words_per_doc_sf_dae = get_avg_words_per_doc_in_dir(sf_dae_dir_path)
    actual_total_compress_sf_dae = float(avg_words_per_doc_origins) / float(avg_words_per_doc_sf_dae)

    if len(SET_REFS_PATH) > 0:
        # Tinh ROUGE-1,L cuoi cung
        print("Avg words per doc in: {}: {} words(s)".format(tr_path, avg_words_per_doc_tr))
        tr_rouge1, tr_rougel = calc_rouges(tr_path, SET_REFS_PATH)

        print("Avg words per doc in: {}: {} words(s)".format(dae_vs_tr_path, avg_words_per_doc_dae_vs_tr))
        dae_vs_tr_rouge1, dae_vs_tr_rougel = calc_rouges(dae_vs_tr_path, SET_REFS_PATH)

        print("Avg words per doc in: {}: {} words(s)".format(tr_dae_dir_path, avg_words_per_doc_tr_dae))
        tr_dae_rouge1, tr_dae_rougel = calc_rouges(tr_dae_dir_path, SET_REFS_PATH)

        print("Avg words per doc in: {}: {} words(s)".format(sf_path, avg_words_per_doc_sf))
        sf_rouge1, sf_rougel = calc_rouges(sf_path, SET_REFS_PATH)

        print("Avg words per doc in: {}: {} words(s)".format(dae_vs_sf_path, avg_words_per_doc_dae_vs_sf))
        dae_vs_sf_rouge1, dae_vs_sf_rougel = calc_rouges(dae_vs_sf_path, SET_REFS_PATH)

        print("Avg words per doc in: {}: {} words(s)".format(sf_dae_dir_path, avg_words_per_doc_sf_dae))
        sf_dae_rouge1, sf_dae_rougel = calc_rouges(sf_dae_dir_path, SET_REFS_PATH)

        # Report
        REPORT_FILE = os.path.join(root_dir, 'report.txt')
        report(REPORT_FILE, 'SETTINGS:')
        report(REPORT_FILE, '\tName: {}'.format(SET_NAME))
        report(REPORT_FILE, '\tPreprocess Mode: {}'.format(SET_PREPROCESS_MODE))
        report(REPORT_FILE, '\tTotal compress: {}'.format(SET_TOTAL_COMPRESS))
        report(REPORT_FILE, '\tExtract compress: {}'.format(SET_EXTRACT_COMPRESS))
        report(REPORT_FILE, '\tOrigins dir: {}'.format(SET_ORIGINS_PATH))
        report(REPORT_FILE, '\tReferences dir: {}'.format(SET_REFS_PATH))
        report(REPORT_FILE, '\tResults dir: {}'.format(SET_RESULTS_PATH))
        report(REPORT_FILE, '\tModel path: {}'.format(SET_MODEL_PATH))

        report(REPORT_FILE, '\tAvg words per origin doc: {}'.format(avg_words_per_doc_origins))
        report(REPORT_FILE, '\tAvg words per ref doc: {}'.format(avg_words_per_doc_refs))

        report(REPORT_FILE, '\nMô hình TextRank:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(tr_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_tr))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_tr))
        report(REPORT_FILE, '\tRouge-1 F: {:.4f}'.format(tr_rouge1))
        report(REPORT_FILE, '\tRouge-L F: {:.4f}'.format(tr_rougel))

        report(REPORT_FILE, '\nMô hình DAE có độ nén tương đương với mô hình TextRank:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(dae_vs_tr_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_dae_vs_tr))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_dae_vs_tr))
        report(REPORT_FILE, '\tRouge-1 F: {:.4f}'.format(dae_vs_tr_rouge1))
        report(REPORT_FILE, '\tRouge-L F: {:.4f}'.format(dae_vs_tr_rougel))

        report(REPORT_FILE, '\nMô hình GMC (TextRank):')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(tr_dae_dir_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_tr_dae))
        report(REPORT_FILE, '\tĐộ nén thực tế trong pha trích rút: {:.4f}'.format(actual_extract_compress_tr))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_tr_dae))
        report(REPORT_FILE, '\tRouge-1 F: {:.4f}'.format(tr_dae_rouge1))
        report(REPORT_FILE, '\tRouge-L F: {:.4f}'.format(tr_dae_rougel))

        report(REPORT_FILE, '\nMô hình Sentence Features:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(sf_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_sf))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_sf))
        report(REPORT_FILE, '\tRouge-1 F: {:.4f}'.format(sf_rouge1))
        report(REPORT_FILE, '\tRouge-L F: {:.4f}'.format(sf_rougel))

        report(REPORT_FILE, '\nMô hình DAE có độ nén tương đương với mô hình Sentence Features:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(dae_vs_sf_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_dae_vs_sf))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_dae_vs_sf))
        report(REPORT_FILE, '\tRouge-1 F: {:.4f}'.format(dae_vs_sf_rouge1))
        report(REPORT_FILE, '\tRouge-L F: {:.4f}'.format(dae_vs_sf_rougel))

        report(REPORT_FILE, '\nMô hình GMC (Sentence Feature):')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(sf_dae_dir_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_sf_dae))
        report(REPORT_FILE, '\tĐộ nén thực tế trong pha trích rút: {:.4f}'.format(actual_extract_compress_sf))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_sf_dae))
        report(REPORT_FILE, '\tRouge-1 F: {:.4f}'.format(sf_dae_rouge1))
        report(REPORT_FILE, '\tRouge-L F: {:.4f}'.format(sf_dae_rougel))
    else:
        # Report
        REPORT_FILE = os.path.join(root_dir, 'report.txt')
        report(REPORT_FILE, 'SETTINGS:')
        report(REPORT_FILE, '\tName: {}'.format(SET_NAME))
        report(REPORT_FILE, '\tPreprocess Mode: {}'.format(SET_PREPROCESS_MODE))
        report(REPORT_FILE, '\tTotal compress: {}'.format(SET_TOTAL_COMPRESS))
        report(REPORT_FILE, '\tExtract compress: {}'.format(SET_EXTRACT_COMPRESS))
        report(REPORT_FILE, '\tOrigins dir: {}'.format(SET_ORIGINS_PATH))
        report(REPORT_FILE, '\tReferences dir: {}'.format(SET_REFS_PATH))
        report(REPORT_FILE, '\tResults dir: {}'.format(SET_RESULTS_PATH))
        report(REPORT_FILE, '\tModel path: {}'.format(SET_MODEL_PATH))

        report(REPORT_FILE, '\tAvg words per origin doc: {}'.format(avg_words_per_doc_origins))
            
        report(REPORT_FILE, '\nMô hình TextRank:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(tr_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_tr))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_tr))

        report(REPORT_FILE, '\nMô hình DAE có độ nén tương đương với mô hình TextRank:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(dae_vs_tr_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_dae_vs_tr))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_dae_vs_tr))

        report(REPORT_FILE, '\nMô hình GMC (TextRank):')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(tr_dae_dir_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_tr_dae))
        report(REPORT_FILE, '\tĐộ nén thực tế trong pha trích rút: {:.4f}'.format(actual_extract_compress_tr))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_tr_dae))

        report(REPORT_FILE, '\nMô hình Sentence Features:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(sf_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_sf))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_sf))

        report(REPORT_FILE, '\nMô hình DAE có độ nén tương đương với mô hình Sentence Features:')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(dae_vs_sf_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_dae_vs_sf))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_dae_vs_sf))

        report(REPORT_FILE, '\nMô hình GMC (Sentence Feature):')
        report(REPORT_FILE, '\tThư mục tóm tắt: {}'.format(sf_dae_dir_path))
        report(REPORT_FILE, '\tSố từ trung bình mỗi văn bản: {} từ'.format(avg_words_per_doc_sf_dae))
        report(REPORT_FILE, '\tĐộ nén thực tế trong pha trích rút: {:.4f}'.format(actual_extract_compress_sf))
        report(REPORT_FILE, '\tĐộ nén thực tế cuối cùng: {:.4f}'.format(actual_total_compress_sf_dae))
    
    ###### 
    print('== END ==')